Datum: 29.04.2021  
Start: 20:05 Uhr  
Ende : 22:16 Uhr

Ort: Online, https://meet.f3n-ac.de/F3N-MV-2021

Protokoll: Sarah Beschorner

---

__Anwesend__, 
- 13 Mitglieder persönlich per Videokonferenz, 0 Mitglieder in Vertretung
- Eindeutige Identifikation aller Anwesenden durch mindestens ein
  Vorstandmitglied per Video erfolgt
  - Anwesenheitsliste:
    - Peter Kämmerling
    - Arne Bayer
    - Felix Preuschoff
    - Malte Möller
    - Sarah Beschorner
    - Dennis Crakau
    - Stefan Limbach
    - Christoph Hüffelmann
    - Christian Bricart
    - Daniel Müllers
    - Matthias Schmidt
    - Jan Niehusmann
    - Christoph Franzen
---

# [TOP 1] Begrüßung

Sarah Beschorner begrüßt alle Anwesenden und heißt sie zu der
Mitgliederversammlung 2021 des F3N Aachen e.V. herzlich willkommen.

# [TOP 2] Formalia

Es wird zu Beginn die Feststellung der Ordnungsmäßigkeit der Einladung zur
Mitgliederversammlung angesprochen. Die Einladung erfolgte frist- und
formgerecht.  
Die Mitgliederversammlung ist laut Satzung ohne Rücksicht auf die Zahl der
erschienenen Mitglieder beschlussfähig. Es sind 13 von 30 Mitgliedern anwesend.
Sarah Beschorner schlägt Dennis Crakau als Sitzungsleiter für die
Mitgliederversammlung vor. Es gibt innerhalb der Versammlung keine
Gegenvorschläge.

Einstimmig wird mit folgendem Ergebnis Dennis Crakau zum Sitzungsleiter
gewählt:

Dafür:        13  
Dagegen:      0  
Enthaltungen: 0

Danach wird Sarah Beschorner als protokollführende Person für die
Mitgliederversammlung durch Sarah Beschorner vorgeschlagen. Es gibt keinen
Gegenvorschlag.

Sarah Beschorner wird mit folgendem Ergebnis zur protokollführenden Person
gewählt:

Dafür:        13  
Dagegen:      0  
Enthaltungen: 0


# [TOP 3] Bericht des Vorstands

Der Jahresbericht 2020 des Vereins wird mit den anwesenden Mitgliedern
durchgegangen und besprochen. Der detaillierte  Vorstandsbericht kann unter
https://gitlab.com/f3n/vorstand/organisation/-/raw/master/Mitgliederversammlung/2021/jahresbericht2020.txt
nachgelesen werden
Peter Kämmerling berichtet von den stattgefundenen Linux Presentation Days.
Hieran hatte der F3N teilweise teilgenommen.


# [TOP 4] Bericht der Kassenprüfer

Der Kassenbericht wird vorgestellt und der Verein kann folgende Bilanz
vorweisen:
- Einnahmen Gesamt:     17.894,00€
- Ausgaben Gesamt:      21.679,49€
- Gesamtvermögen (neu): 10.493,83€

Ein detailierter Kassenbericht wurde mit der Einladung an die Mitglieder
verschickt und kann unter folgendem Link eingesehen werden:  
https://gitlab.com/f3n/info/-/blob/master/mv2021/Kassen_und_Pr%C3%BCfbericht_2020.txt

Die Kassenprüfer werden zur Kassenführung und -prüfung befragt. 
Daniel Müllers, Christoph Franzen und Felix Preuschoff geben an, bei der 
Überprüfung eine gut geführte und korrekte Kasse vorgefunden zu haben.
Sie empfehlen den Kassenwart bzw. den Vorstand zu entlasten.
Peter Kämmerling merkt an, dass Spenden nach ihrem Zweck aufgeschlüsselt werden 
sollen, sobald eine Gemeinnützigkeit vorliegt.


# [TOP 5] Entlastung des Vorstands

Auf Grund des Kassen- und Jahresberichts wird mit folgendem Ergebnis über die
Entlastung des Vorstands abgestimmt:

Dafür:        7
Dagegen:      0
Enthaltungen: 6


# [TOP 6] Wahl des Vorstands

Der alte Vorstand stellt sich in der ursprünglichen Konstellation zur
vollständigen Wiederwahl auf:

- Jan Niehusmann stellt sich zur Wahl als Kassenwart bereit
- Sarah Beschorner stellt sich zur Wahl als Vorsitzende bereit
- Dennis Crakau stellt sich zur Wahl als Vorsitzender bereit
- Malte Möller stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit
- Arne Bayer stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit
- Matthias Schmidt stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit

## Wahl - 1. Vorsitz

Kandidatin: Sarah Beschorner 
Dafür:        12
Dagegen:      0  
Enthaltungen: 1

Sarah Beschorner nimmt die Wahl an.

20:27Uhr: Jan Niehusmann übernimmt für die Wahl des 2. Vorsitzenden die
Sitzungsleitung.

## Wahl - 2. Vorsitz

Kandidat: Dennis Crakau

Dafür:        12  
Dagegen:      0  
Enthaltungen: 1

Dennis Crakau nimmt die Wahl an.

20:28Uhr: Dennis Crakau übernimmt wieder die Sitzungsleitung.

## Wahl - Kassenwart

Kandidat: Jan Niehusmann 

Dafür:        12 
Dagegen:      0  
Enthaltungen: 1

Jan Niehusmann nimmt die Wahl an.

## Wahl - Beisitzer

Kandidat: Malte Möller

Dafür:        12  
Dagegen:      0  
Enthaltungen: 1

Malte Möller nimmt die Wahl an.

## Wahl - Beisitzer

Kandidat: Arne Bayer

Dafür:        12  
Dagegen:      0  
Enthaltungen: 1

Arne Bayer nimmt die Wahl an.

## Wahl - Beisitzer

Kandidat: Matthias Schmidt 

Dafür:        12  
Dagegen:      0  
Enthaltungen: 1

Matthias Schmidt nimmt die Wahl an.

## Wahl - Kassenprüfung

Es werden KandidatInnen für die Kassenprüfung für das nächste Geschäftsjahr
vorgeschlagen:
- Felix Preuschoff
- Daniel Müllers
- Christoph Franzen

Es wird über alle drei Kandidaten gemeinsam abgestimmt.
Ergebnis der Wahl zur Kassenprüfung:

Dafür:        10 
Dagegen:      0  
Enthaltungen: 3

Felix Preuschoff und Daniel Müllers nehmen die Wahl an.
Christoph Franzen hatte Verbindungsprobleme und erhält separat für die Annahme 
der Wahl eine E-Mail. 
Christoph Franzen nimmt per Mail an.


# [TOP 7] Berufung von Teams

## 7.1 Aktuell vorhandene Teams (aus MV19):

### Team Düren

- Budget aus Eigenmitteln
- Mitglieder: Markus Jakobs, Ralf Pütz, Emir,  Matthias Schmidt
- Bericht des Teams:
  - Matthias
  - Für weitere Information: kontakt@freifunk-dueren.de

### Team Server/ Backbone

- Budget aus städtischen Mitteln: 4.000€ (2018), 10.000€ (2019), 12.000€ (2020)
  (prüfen)
- Mitglieder: Christian Bricart, Arne Bayer, Daniel Müllers, Benedikt Hoss, Jan
  Niehusmann, Malte Möller
- Bericht des Teams:
  - Malte, Christian Bricart
  - Für weitere Information: technik@freifunk-aachen.de 

### Team Herzogenrath

- Budget aus Eigenmitteln + Förderungen
- Mitglieder: Volker Behrendt, Thomas Göttgens
- Bericht des Teams:
  - keine Mitglieder sind in der MV anwesend für einen Bericht


## 7.2 Geplante Änderungen in aktuellen Teams

keine


## 7.3 Vorgeschlagene, neue Teams

### Freifunk Welthaus

- Budget aus Staatskanzlei NRW + Eigenmitteln
- Mitglieder: Peter Kämmerling, Malte Möller, Daniel Müllers

Wahl des neuen Teams Freifunk Welthaus:
Dafür:        10  
Dagegen:      0 
Enthaltungen: 3


## 7.4 Erhaltung oder Auflösung der vorhandenen Teams 

Sarah Beschorner und Jan Niehusmann regen an das Team Herzogenrath
aufzulösen. Christoph Hüffelmann und Malte Möller möchten jedoch das Team
weiter erhalten, da durch die Corona-Situation Aktivitäten und die Teilnahme an
der heutigen Mitgliederversammlung erschwert werden.
Es wird vorgeschlagen das Team nochmal per Mail zu kontaktieren und mehr
Informationen über die aktuellen Tätigkeiten zu erhalten.
Eine Auflösung des Teams wird hiermit verschoben.

Allgemein schlägt Peter Kämmerling vor, dass die Teams bei der
Mitgliederversammlung von ihren Erfolgen berichten sollen. 
Jan Niehusmann bestätigt, dass die Teams vor der nächsten
Mitgliederversammlung einzeln per Mail kontaktiert werden sollen, damit von
jedem Team eine Person anwesend ist. Hiermit wird zukünftig sichergestellt,
dass aus jedem Team eine Person berichten kann. 


# [TOP 8] Geplante Aktivitäten

Folgende Aktivitäten des F3N sind für das laufende Geschäftsjahr (2021)
geplant:
- Endabrechnung Förderantrag zur Anschaffung eigener Server
- Aufbau Richtfunk Backbone Aachen
- Als erstes Projekt ist die Anbindung des Welthaus Aachen und Umgebung in der
  Planungsphase
- Durchführung von Veranstaltungen und Workshops zur Wissensweitergabe


# [TOP 9] Gemeinnützigkeit

Christoph Franzen kann aus technischen Gründen nicht mehr an der 
Mitgliederversammlung teilnehmen. (21:20)

Nach den neusten Änderungen von der Bundesregierung, ist es Freifunk-Vereinen 
durch eine Änderung der Abgabenordnung neuerdings leichter möglich, als 
gemeinnützig anerkannt zu werden.
Hierfür ist eine Satzungsänderung nötig.
Diese Möglichkeit wird innerhalb der Mitgliederversammlung mit allen Anwesenden
diskutiert. Über die Satzungsänderung ist in der Mitgliederversammlung
abzustimmen, es ist eine Mehrheit von 2/3 der anwesenden ordentlichen
Mitglieder notwendig.

Jan Niehusmann merkt an, dass Mitgliedsbeiträge wahrscheinlich nicht abgesetzte
werden können, sondern nur Spenden. Eine Gemeinnützigkeit  würde das Erhalten
von Spenden vereinfachen. Nachteile bei einer Gemeinnützigkeit sind, dass der
Verein an die entsprechenden Regelungen der Abgabenordnung gebunden ist, sich
an die Bedingungen der Mittelverwendung halten muss und die Buchhaltung
komplexer wird.
Jan Niehusmann führt an, dass er bei einem Entscheid für die Gemeinnützigkeit 
die Hilfe eines/r Steuerberaters/Steuerberaterin für die Verwaltung der Kasse 
in Anspruch nehmen würde.
Es wurde beim Finanzamt angefragt, welche Änderungen benötigt werden. Dies kann
durch einen kleinen Zusatz in der Satzung §3 "Zweck des Vereins" ermöglicht
werden.

Beschluss, ob er Verein die Gemeinnützigkeit anstreben soll:
Dafür:        7  
Dagegen:      0
Enthaltungen: 5

In die Satzung soll der folgende Satz an §3 "Zweck des Vereins"
angefügt werden: "Der Verein verfolgt ausschließlich und unmittelbar
gemeinnützige Zwecke im Sinne des Abschnitts "Steuerbegünstigte Zwecke" der
Abgabenordnung."
 
Beschluss zur Satzungsänderung für die Gemeinnützigkeit:
Dafür:        12  
Dagegen:      0
Enthaltungen: 0


# [TOP 10] Verschiedenes

Peter Kämmerling führt weitere Ideen und Pläne zum Projekt Welthaus aus. 
Das Budget und die konkrete Planung des Projekt kann in der aktuellen Version
(V6) eingesehen werden. Das aktuelle kalkulierte Budget beträgt 100.000€. 
In der Slack-Gruppe '#allgemein' können weitere Informationen gefunden werden. 

Peter Kämmerling stellt zur Abstimmung, dass das Projekt Welthaus von der MV
des F3N e.V. unterstützt wird.
Vor Abgabe der Endfassung des Antrags wird zwecks Abstimmung hierüber
eine außerordentliche MV einberufen.

Beschluss zur Unterstützung des Projekts Freifunk Welthaus:
Dafür:        12 
Dagegen:      0
Enthaltungen: 0


# [TOP 11] Abschluss der Versammlung

Dennis bedankt sich für die Mitarbeit und verabschiedet sich bei allen
Anwesenden.

Ende der MV um 22:16 Uhr.

---
<br>
<br>
(Sarah Beschorner)<br>
Protokoll
<br>
<br>
<br>
(Dennis Crakau)<br>
Sitzungsleitung
<br>
<br>
<br>
(Jan Niehusmann)<br>
Sitzungsleitung Wahl 2. Vorsitzender

---
