Hallo {{Vorname}},

am 11.4.2019 findet die diesjährige Mitgliederversammlung der
Fördervereinigung für freie Netzwerke in der Region Aachen e.V. statt.

Die Versammlung beginnt um 20:00 Uhr in der 'digitalCHURCH', Jülicher
Strasse 72a in Aachen im Sitzungsraum "Great Conference".
Als Tagesordnung steht auf dem Programm:

1) Begrüßung
2) Formalia
3) Bericht des Vorstands
4) Bericht der Kassenprüfer
5) Entlastung des Vorstands
6) Wahl des Vorstands
7) Berufung von Teams
8) Abstimmung über Satzungsänderungen
9) Abstimmung über Geschäfts-, Finanz- und Beitragsordnungen
10) Geplante Aktivitäten
11) Verschiedenes
12) Abschluss der Versammlung

Zu den Tagesordnungpunkten von unserer Seite folgende Hinweise:

3) Bericht des Vorstands
Ist in Ausarbeitung, wird vor der MV zugestellt.

4) Bericht der Kassenprüfer
Satzungsgemäß im Anhang

6) Wahl des Vorstandes
Freiwillige gerne melden, es gibt genug zu tun.

7) Berufung von Teams
Plant ihr Projekte für die es noch keine Teams gibt? Meldet euch!

8) Abstimmung über Satzungsänderungen
Es wurden folgende Satzungsänderungsvorschläge eingereicht:

   - §1 Name und Sitz des Vereins: In Punkt 1 wird die Abkürzung
     "F3N" ergänzt, sodass er vollständig lautet:

          Der Verein führt den Namen „Fördervereinigung für freie
          Netzwerke in der Region Aachen, abgekürzt „F3N" oder
          „F3N Aachen".

   - §13 (7) und §17 (5) der aktuellen Satzung widersprechen
     sich. Daher wird §13 (7) "Soll der Vorstand entlastet werden, so
     ist der Einladung ein Kassenprüfbericht der Kassenprüfer*innen
     beizulegen." gestrichen.

     Die geschlechtergerechte Sprache aus §13 (7) wird in §17 (5)
     übernommen, der dann lautet:

          Soll der Vorstand entlastet werden, ist der
          Mitgliederversammlung zwei Wochen vor deren Zusammentreten
          ein Kassenprüfbericht von zwei Kassenprüfer*innen vorzulegen.

   - Vollmacht für Stimmrecht soll nur an andere ordentliche Mitglieder
     möglich sein. Dies wird in §13 (13) ergänzt, der dann lautet:

          Jedes ordentliche Mitglied hat eine Stimme. Das Stimmrecht
          kann nur persönlich oder durch ein anderes ordentliches
          Mitglied unter Vorlage einer schriftlichen Vollmacht ausgeübt
          werden.

Wir freuen uns auf Euer zahlreiches Erscheinen.

für den Vorstand
Arne Bayer
Gregor Bransky
Dennis Crakau
Malte Möller
Jan Niehusmann


Anhang:

Kassenbericht, Stand 31.12.2018
-------------------------------

- Einnahmen aus Mitgliedsbeiträgen                      1116,00€
  (bisher haben 21 von 27 Mitgliedern bezahlt, davon 3* ermäßigt)
- Einnahmen durch Schenkungen:                           720,00€
- Einnahmen durch öffentliche Förderung:                8000,00€
- Einnahmen Freifunktag:                                1256,10€

- Einnahmen Gesamt:                                    11092,10€

- Ausgaben für die Gründung:                             140,45€
- Ausgaben für Infrastruktur (Mailserver, Domains):       47,98€
- Ausgaben Freifunktag:                                 2988,90€
- Ausgaben 'Erstattung Veranstaltungsteilnahmen':        695,41€
- Ausgaben Internet:                                     952,00€
- Ausgaben Material:                                     347,71€

- Ausgaben Gesamt:                                      5172,45€
- Darin enthaltene offene Verbindlichkeiten:         (-) 798,12€
- Bereits erfolgte Ausgaben:                            4374 33€

Damit ergibt sich als aktueller Kassenstand (und gleichzeitig Guthaben
auf unserem Bankkonto) ein Betrag von                   6717,77€

- Zzgl.  offene Forderungen Mitgliedsbeiträge:           264,00€
- Abzgl. offene Verbindlichkeiten:                   (-) 798,12€

- Gesamtvermögen                                        6183,65€

Verwendung zweckgebundener Mittel
---------------------------------

Von den Einnahmen unterlagen 8000€ einer engeren Zweckbindung als schon durch
die Satzung vorgegeben (Ratsantrag 330/17, 22.03.2018):
"Diese Mittel sollen für die Förderung der Aktivitäten des Vereins,
insbesondere für Betrieb und Wartung notwendiger Infrastrukur beim
Einsatz von Freifunk sowie zur Kostendeckung der Unterstützung der
Stadt Aachen bei der Ausleuchtung öffentlicher Einrichtungen dienen."

Hiervon wurden bezahlt:

- Ausgaben für Infrastruktur (Mailserver, Domains):       47,98€
- Ausgaben abzgl. Einnahmen Freifunktag:                1732,80€
- Ausgaben 'Erstattung Veranstaltungsteilnahmen':        695,41€
- Ausgaben Internet:                                     952,00€
                                                        ========
                                                        3428,19€

Vom Vermögen noch der Zweckbindung unterliegend         4571,81€
Freies Vermögen                                         1611,84€




Jan Niehusmann (Kassenwart)


Prüfbericht der Kassenprüfer
----------------------------

Der schriftliche Kassenbericht des Vereins wurden von den in der
Mitgliederversammlung gewählten Kassenprüfern geprüft. Dabei wurden
auch die weiteren Unterlagen und die vorgelegten Belege eingesehen.

Übereinstimmung besteht auch bei den vorgelegten Vereins-Bankauszügen
und dem Abgleich mit den vollständig vorliegenden Belegen.

Es wurden keine Beanstandungen festgestellt.

