Datum: 11.04.2019  
Start: 20:10 Uhr  
Ende : 22:27 Uhr

Ort: DIGITAL CHURCH, Jülicher Str. 72a, 52070 Aachen

Protokoll: Sarah Beschorner

---

__Anwesend__
- 16 Mitglieder persönlich, 0 Mitglieder in Vertretung
  - siehe Anwesenheitsliste

---

# [TOP 1] Begrüßung
Dennis Crakau begrüßt alle Anwesenden und heißt sie zu der
Mitgliederversammlung 2019 des Fördervereins für freie Netzwerke e.V. herzlich
willkommen.


# [TOP 2] Formalia

Es wird zu Beginn die Feststellung der Ordnungsmäßigkeit der Einladung zur
Mitgliederversammlung angesprochen. Die Einladung erfolgte frist- und
formgerecht.

Die Mitgliederversammlung ist laut Satzung ohne Rücksicht auf die Zahl der
erschienenen Mitglieder beschlussfähig. Es sind 16 von 26 Mitgliedern anwesend.

Jan Niehusmann schlägt Dennis Crakau als Sitzungsleiter für die
Mitgliederversammlung vor. Es gibt innerhalb der Versammlung keine
Gegenvorschläge.  

Dennis wird mit folgendem Ergebnis zum Sitzungsleiter gewählt:

- Dafür:        15
- Dagegen:       0
- Enthaltungen:  1

Danach wird Sarah Beschorner als Protokollführerin für die
Mitgliederversammlung durch Jan Niehusmann vorgeschlagen. Es gibt keinen
Gegenvorschlag.  

Sarah Beschorner wird mit folgendem Ergebnis zur Protokollführerin gewählt:

- Dafür:       15
- Dagegen:      0
- Enthaltungen: 1


# [TOP 3] Bericht des Vorstands

Der Jahresbericht 2018 des Vereins wird mit den anwesenden Mitglieder
durchgegangen und besprochen.

Gregor Bransky gibt Bedenken zu der Auflistung der Aktivitäten innerhalb der
Freifunk-Community an und möchte gerne den Absatz "Gemäß §3...
Freifunk-Community Aachen" aus dem Jahrebericht streichen. Als Begründung führt
er an, dass der F3N sich bezüglich öffentlicher Arbeit rund um die Community im
Hintergrund halten sollte. Aktivitäten, die durch die Freifunk Aachen Community
ausgeführt wurden, sollten aus dem Bericht gestrichen werden, da sie im
eigentlichen Sinne keine Aktivitäten des Vereins sind.  
Durch die geforderte Streichung des Absatzes entfallen Informationen zur
"Anschaffung der Werbe- und Infomaterialien", sowie Vortragsthemen auf dem
35C3, welche dem Absatz der Ziele hinzugefügt werden sollen.  
Über die Änderungen des Jahresberichts wurde mit folgenden Ergebnis abgestimmt:

- Dafür:        15
- Dagegen:       0
- Enthaltungen:  1

Der angenommene veränderte Jahresbericht kann unter folgendem Link eingesehen
werden:  
https://gitlab.com/f3n/vorstand/organisation/raw/master/Mitgliederversammlung/2019/Jahresbericht_2018.txt


# [TOP 4] Bericht der Kassenprüfer

Der Kassenbericht wird vorgestellt und der Verein kann folgende Bilanz
vorweisen:

- Einnahmen gesamt: 11.092,10€
- Ausgaben gesamt:   5.172,45€

Ein detailierter Kassenbericht kann unter folgendem Link eingesehen werden:  
https://gitlab.com/f3n/vorstand/organisation/raw/master/Mitgliederversammlung/2019/Kassen_und_Pr%C3%BCfbericht_2018.txt

Die Kassenprüfer werden zur Kassenführung und -prüfung befragt. Oliver Huberty
und Christoph Franzen geben an, bei der Überprüfung eine gut geführte und
korrekte Kasse vorgefunden zu haben und empfehlen, den Kassenwart bzw. den
Vorstand zu entlasten.


# [TOP 5] Entlastung des Vorstands

Auf Grund des Kassens- und Jahresberichts wird mit folgendem Ergebnis über die
Entlastung des Vorstands abgestimmt.

- Dafür:        11
- Dagegen:       0
- Enthaltungen:  5


# [TOP 6] Wahl des Vorstands

Der alte Vorstand stellt sich nicht mehr in der ursprünglichen Konstellation
zur vollständigen Wiederwahl auf.

Der Vorstand möchte vor der eigentlichen Vorstandswahl auf Grund von
Kommunikations- und persönlichen Schwierigkeiten von Malte Möller und Gregor
Bransky als Meinungsbild eine Stichwahl zwischen den beiden Mitgliedern
durchführen lassen.

Die Sitzung wird vor der Wahl um 21:21 Uhr unterbrochen und um 21:37 Uhr
weitergeführt.

Es wird für die Stichwahl eine Zählkommission aus Thomas Neugebauer und Daniel
Müllers gebildet. Die Stichwahl findet geheim statt.

Die Stichwahl führt zu folgendem Ergebnis:  
- Malte:        7 Stimmen  
- Gregor:       3 Stimmen  
- Keine Person: 2 Stimmen  
- Enthaltung:   4 Stimmen  

Nach der Stichwahl gibt Gregor Bransky an, nicht mehr für den Vorstand des F3N
kandidieren zu wollen.  
Malte Möller möchte nicht mehr als Vorsitzender kandidieren und sich mehr den
technischen Themen widmen. Daher schlägt Malte Möller Sarah Beschorner als 
mögliche Kandidatin für den Vorsitz vor. 
Er gibt an, dass Sarah Beschorner und er in einer eheähnlichen Beziehung 
leben.
Das Thema wurde zur Diskussion gestellt, falls Mitglieder Bedenken bezüglich 
dieser Aufstellung haben. Es wurde keine Diskussion als nötig befunden.

Folgende Personen werden für den neuen Vorstand vorgeschlagen:

- Jan Niehusmann stellt sich zur Wahl als Kassenwart bereit
- Sarah Beschorner stellt sich zur Wahl als Vorsitzende bereit
- Dennis Crakau stellt sich zur Wahl als Vorsitzender bereit
- Malte Möller stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit
- Arne Bayer stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit
- Matthias Schmidt stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit 

Daniel Müllers übernimmt für die Wahl des Vorstandes die Sitzungsleitung.

- Wahl - 1. Vorsitz- Kandidatin: Sarah Beschorner:
  - Dafür:        15
  - Dagegen:       0
  - Enthaltungen:  1

Sarah Beschorner nimmt die Wahl an.


- Wahl - 2. Vorsitz - Kandidat: Dennis Crakau:
  - Dafür:        15
  - Dagegen:       0
  - Enthaltungen:  1

Dennis Crakau nimmt die Wahl an.


- Wahl - Kassenwart - Kandidat: Jan Niehusmann:
  - Dafür:        15
  - Dagegen:       0
  - Enthaltungen:  1

Jan Niehusmann nimmt die Wahl an.


- Wahl - Beisitzer - Kandidat: Malte Möller
  - Dafür:        12
  - Dagegen:       1
  - Enthaltungen:  3

Malte Möller nimmt die Wahl an.


- Wahl - Beisitzer - Kandidat: Arne Bayer
  - Dafür:        13
  - Dagegen:       0
  - Enthaltungen:  3

Arne Bayer nimmt wie Wahl an.


- Wahl - Beisitzer - Kandidat: Matthias Schmidt
  - Dafür:        13
  - Dagegen:       0
  - Enthaltungen:  3
	

Nach der Wahl des Vorstands wird die Sitzungsleitung wieder von Dennis Crakau
übernommen.

Es werden Kandidaten/Kandidatinnen für die Kassenprüfung für das nächste
Geschäftsjahr vorgeschlagen:
- Oliver Huberty
- Christop Franzen
- Daniel Müllers

Es wird über alle drei Kandidaten gemeinsam abgestimmt.

Ergebnis der Wahl zur Kassenprüfung:
- Dafür:        13
- Dagegen:       0
- Enthaltungen:  3


Jan Niehusmann bedankt sich im Namen des ehemaligen Vorstands bei Gregor
Bransky für die gute Zusammenarbeit und hofft, ihn noch weiter bei den Treffen
begrüßen zu können. Gregor möchte gerne eine Übergabe mit dem neuen Vorstand
organisieren.


# [TOP 7] Berufung von Teams

## 1) Aktuell vorhandene Teams (aus MV18):

- Team Düren
  - Budget aus Eigenmitteln
  - Mitglieder: Markus Jakobs, Ralf Pütz, Emir,  Matthias Schmidt
  - Bericht des Teams:
    - Lokaler Netzausbau
    - Eigene Infrastrukur für Projektorganisation
    - Wöchentliches Freifunk Beratungsangebot in Düren
    - hyperlokale Freifunkdienste
    
- Team Freifunktag
  - Budget aus Eigenmitteln und 2.500€ für 2018
  - Mitglieder: Benedikt Hoss, Gregor Bransky, Friederike Nitz
  - Bericht des Teams:
    - Organisation & Durchführung des ersten Freifunktags in Aachen in der
      DIGITAL CHURCH
      
- Team Server/ Backbone
  - Budget aus städtichen Mitteln: 4.000€ (2018), 10.000€ (2019)
  - Mitglieder: Christian Bricart, Arne Bayer, Daniel Müllers, Benedikt Hoss,
    Jan Niehusmann, Malte Möller
  - Bericht des Teams:
    - Kostenermittlung & Bezahlung einer HE im Rechenzentrum mit 225MBit/s
      Bandbreite zur Schließung der Lücke, welche nicht durch Sachspenden
      gedeckt ist.
    - Ermittlung & Erstellung einer Liste zur Beschaffung eigener Server
    - Inbetriebnahme 3. Server Hardware
    - Vollständige Umstellung unsere Infrastruktur auf dedizierte Server
    - öffentliche Dokumentation der VM auf diesen Servern auf gitlab
    
- Team Baesweiler (Beggendorf)
  - Budget aus Eigenmitteln + Förderungen
  - Mitglieder: Rüdiger Emmler, Andreas Dorfer, Oliver Huberty,
    Gregor Bransky, Dennis Crakau
  - Bericht des Teams:
    - Planung eines möglichst Ortsumspannenden Freifunk Netzwerks, Klärung
      mit der Stadt
    - Finanzierung über Spenden 
      - derzeit Stillstand, weil eine Schule abgerissen wird, die
        ursprünglich als Einspannpunkt angedacht war.


## 2) Geplante Änderungen in aktuellen Teams

Es sind keine Änderungen in den Teams vorgesehen. Das Team Freifunktag bleibt
für weitere Organisationen innerhalb Aachens bestehen.
 
 
## 3) Vorgeschlagene, neue Teams

- Team Herzogenrath 
  - Budget aus Eigenmitteln + Förderungen
  - Mitglieder: Volker Behrendt, Thomas Göttgens

Es wird über das vorgeschlagene Team Herzogenrath abgestimmt: 
- Dafür:        16
- Dagegen:       0
- Enthaltungen:  0 


# [TOP 8] Abstimmung über Satzungsänderungen

## Änderungsvorschlag 1)

Aufgrund eines Einwandes, die Abkürzung "F3N" alleine könne zu
Verwechslungsgefahr führen, die Abkürzung "F3N Aachen" sei diesbezüglich
sinnvoller, wird die Abkürzung "F3N" alleine aus dem Änderungsantrag gestrichen.
Es wird darüber abgestimmt, in §1 (1) der Satzung die Abkürzung "F3N Aachen"
einzufügen, sodass der Satz vollständig lautet:
    
    Der Verein führt den Namen „Fördervereinigung für freie Netzwerke in der
    Region Aachen", abgekürzt „F3N Aachen".


- Dafür:        16
- Dagegen:       0
- Enthaltungen:  0


## Änderungsvorschlag 2)

    §13 (7) und §17 (5) der aktuellen Satzung widersprechen sich. Daher wird
	§13 (7) "Soll der Vorstand entlastet werden, so ist der Einladung ein
	Kassenprüfbericht der Kassenprüfer*innen beizulegen." gestrichen.

    Die geschlechtergerechte Sprache aus §13 (7) wird in §17 (5) übernommen,
	der dann lautet:

    Soll der Vorstand entlastet werden, ist der Mitgliederversammlung zwei
	Wochen vor deren Zusammentreten ein Kassenprüfbericht von zwei
	Kassenprüfer*innen vorzulegen.


- Dafür:        16
- Dagegen:       0 
- Enthaltungen:  0 


## Änderungsvorschlag 3)

    Vollmacht für Stimmrecht soll nur an andere ordentliche Mitglieder möglich
	sein. Dies wird in §13 (13) ergänzt, der dann lautet:

    Jedes ordentliche Mitglied hat eine Stimme. Das Stimmrecht kann nur
	persönlich oder durch ein anderes ordentliches Mitglied unter Vorlage einer
	schriftlichen Vollmacht ausgeübt werden.


- Dafür:        16
- Dagegen:       0 
- Enthaltungen:  0


# [TOP 9] Abstimmung über Geschäfts-, Finanz- und Beitragsordnungen

Es liegen keine beschlussfähigen Ordnungen zur Abstimmung vor.


# [TOP 10] Geplante Aktivitäten
Folgende Aktivitäten des F3N sind für das nächste Jahr geplant:
- Beschaffung eigener Server-Hardware zur Sicherung der Netzstabilität
- Verhandlung mit einem weiteren Rechenzentrum zur Traffic-Durchleitung
   zur Optimierung von Datenflüssen


# [TOP 11] Verschiedenes
Gregor gibt an sich auf EU-Ebene und im europäischen Parlaments für Freifunk
engagieren zu wollen und dort vor der Politik über das Thema Wifi4EU beratend
tätig zu sein. Dieser Tätigkeit möchte er im Kontext des Förderverein Freie
Netzwerke e. V., Berlin, mit Monic Meisel gemeinsam nachgehen.


# [TOP 12] Abschluss der Versammlung

Dennis Crakau bedankt sich für die Mitarbeit und verabschiedet sich bei allen
Anwesenden.

Ende der MV um 22:27 Uhr.

<br />
<br />
<br />
<br />
<br />
(Sarah Beschorner/ Protokollführerin)          

<br />
<br />
<br />
<br />
<br />
(Dennis Crakau/ Sitzungsleitung)

<br />
<br />
<br />
<br />
<br />
(Daniel Müllers/ Wahlleiter)
