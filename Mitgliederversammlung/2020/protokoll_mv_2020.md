Datum: 28.05.2020
Start: 20:10 Uhr
Ende : 21:10 Uhr

Ort: Online, https://meet.f3n-ac.de/mv2020

Protokoll: Sarah Beschorner

---

__Anwesend__
- 14 Mitglieder persönlich per Videkonferenz, 0 Mitglieder in Vertretung
- Eindeutige Identifikation aller Anwesenden durch mindestens ein
  Vorstandmitglied per Video erfolgt
  - Anwesenheitsliste:
    - Sarah Beschorner
    - Malte Möller
    - Daniel Müllers
    - Thomas Neugebauer
    - Jan Niehusmann
    - Dennis Crakau
    - Mario Dohlen
    - Felix Preuschoff
    - Christoph Hüffelmann
    - Christian Bricart
    - Arne Bayer
    - Benedikt Hoß
    - Christoph Franzen
    - Matthias Schmidt

---

# [TOP 1] Begrüßung

Sarah Beschorner begrüßt alle Anwesenden und heißt sie zu der
Mitgliederversammlung 2020 des F3N Aachen e.V. herzlich willkommen.


# [TOP 2] Formalia

Es wird zu Beginn die Feststellung der Ordnungsmäßigkeit der Einladung zur
Mitgliederversammlung angesprochen. Die Einladung erfolgte frist- und
formgerecht.  
Die Mitgliederversammlung ist laut Satzung ohne Rücksicht auf die Zahl der
erschienenen Mitglieder beschlussfähig. Es sind 14 von 29 Mitgliedern anwesend.
Sarah Beschorner schlägt Dennis Crakau als Sitzungsleiter für die
Mitgliederversammlung vor. Es gibt innerhalb der Versammlung keine
Gegenvorschläge.

Einstimmig wird mit folgendem Ergebnis zum Sitzungsleiter gewählt:

Dafür: 14  
Dagegen: 0  
Enthaltungen: 0

Danach wird Sarah Beschorner als protokollführende Person für die
Mitgliederversammlung durch Sarah Beschorner vorgeschlagen. Es gibt keinen
Gegenvorschlag.

Sarah Beschorner wird mit folgendem Ergebnis zur protokollführenden Person
gewählt:

Dafür: 14  
Dagegen: 0  
Enthaltungen: 0


# [TOP 3] Bericht des Vorstands

Der Jahresbericht 2019 des Vereins wird mit den anwesenden Mitglieder
durchgegangen und besprochen.
Der detailierte Vorstandsbericht kann unter
https://gitlab.com/f3n/vorstand/organisation/-/raw/master/Mitgliederversammlung/2020/jahresbericht2020.txt
nachgelesen werden


# [TOP 4] Bericht der Kassenprüfer

Der Kassenbericht wird vorgestellt und der Verein kann folgende Bilanz
vorweisen:
- Einnahmen Gesamt: 13.746,57€
- Ausgaben Gesamt: 6.751,49€

Ein detailierter Kassenbericht wurde mit der Einladung an die Mitglieder
verschickt und kann unter folgendem Link eingesehen werden:  
https://gitlab.com/f3n/info/-/blob/master/mv2020/Kassen_und_Pr%C3%BCfbericht_2019.txt

Die Kassenprüfer werden zur Kassenführung und -prüfung befragt. Daniel Müllers
und Christoph Franzen geben an, bei der Überprüfung eine gut geführte und
korrekte Kasse vorgefunden zu haben und empfehlen, den Kassenwart bzw. den
Vorstand zu entlasten.


# [TOP 5] Entlastung des Vorstands

Auf Grund des Kassen- und Jahresberichts wird mit folgendem Ergebnis über die
Entlastung des Vorstands abgestimmt:

Dafür: 14  
Dagegen: 0  
Enthaltungen: 0


# [TOP 6] Wahl des Vorstands

Der alte Vorstand stellt sich in der ursprünglichen Konstellation zur
vollständigen Wiederwahl auf:

- Jan Niehusmann stellt sich zur Wahl als Kassenwart bereit
- Sarah Beschorner stellt sich zur Wahl als Vorsitzende bereit
- Dennis Crakau stellt sich zur Wahl als Vorsitzender bereit
- Malte Möller stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit
- Arne Bayer stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit
- Matthias Schmidt stellt sich zur Wahl als Beisitzer (Satzung §14 1.3) bereit

## Wahl - 1. Vorsitz

Kandidatin: Sarah Beschorner

Dafür: 13  
Dagegen: 0  
Enthaltungen: 1

Sarah Beschorner nimmt die Wahl an.

20:23Uhr: Malte Möller übernimmt für die Wahl des 2. Vorsitzenden die
Sitzungsleitung.

## Wahl - 2. Vorsitz

Kandidat: Dennis Crakau

Dafür: 13  
Dagegen: 0  
Enthaltungen: 1

Dennis Crakau nimmt die Wahl an.

20:25Uhr: Dennis Crakau übernimmt wieder die Sitzungsleitung.

## Wahl - Kassenwart

Kandidat: Jan Niehusmann

Dafür: 13  
Dagegen: 0  
Enthaltungen: 1

Jan Niehusmann nimmt die Wahl an.

## Wahl - Beisitzer

Kandidat: Malte Möller

Dafür: 13  
Dagegen: 0  
Enthaltungen: 1
 
Malte Möller nimmt die Wahl an.

## Wahl - Beisitzer

Kandidat:Arne Bayer

Dafür: 13  
Dagegen: 0  
Enthaltungen: 1

Arne Bayer nimmt die Wahl an.

## Wahl - Beisitzer

Kandidat: Matthias Schmidt 

Dafür: 13  
Dagegen: 0  
Enthaltungen: 1

Matthias nimmt die Wahl an.

## Wahl - Kassenprüfung

Es werden KandidatInnen für die Kassenprüfung für das nächste Geschäftsjahr
vorgeschlagen:
- Daniel Müllers
- Felix Preuschoff
- Christoph Franzen

Es wird über alle drei Kandidaten gemeinsam abgestimmt.
Ergebnis der Wahl zur Kassenprüfung:

Dafür: 13  
Dagegen: 0  
Enthaltungen: 1

Daniel, Felix und Christoph nehmen die Wahl an.


# [TOP 7] Berufung von Teams

## 7.1 Aktuell vorhandene Teams (aus MV18):

### Team Düren

- Budget aus Eigenmitteln
- Mitglieder: Markus Jakobs, Ralf Pütz, Emir,  Matthias Schmidt
- Bericht des Teams:
  - Matthias
  - Für weitere Information: kontakt@freifunk-dueren.de

### Team Freifunktag

- Budget aus Eigenmitteln und 2.500€ für 2018
- Mitglieder: Benedikt Hoss, Gregor Bransky, Friederike Nitz

Da der Freifunktag dieses Jahr durch die Corona-Krise abgesagt wurde und das
Team nicht mehr aktiv ist, wird innerhalb der Mitgliederversammlung darüber
abgestimmt, ob das Team aufgelöst werden soll:

Dafür: 12  
Dagegen: 1  
Enthaltungen: 1

Das Team wird aufgelöst.

### Team Server/ Backbone

- Budget aus städtichen Mitteln: 4.000€ (2018), 10.000€ (2019), 12.000€ (2020)
- Mitglieder: Christian Bricart, Arne Bayer, Daniel Müllers, Benedikt Hoss, Jan
  Niehusmann, Malte Möller
- Bericht des Teams:
  - Christian, Arne, Malte
  - Für weitere Information: technik@freifunk-aachen.de 

### Team Baesweiler (Beggendorf)

- Budget aus Eigenmitteln + Förderungen
- Mitglieder: Rüdiger Emmler, Andreas Dorfer, Oliver Huberty, Gregor Bransky,
  Dennis Crakau
- Bericht des Teams:
  - Rüdiger Emmler E-Mail: "Das Projekt Beggendorf kann auch gestoppt werden,
   da der Vorstand der Dorfwerkstatt Beggendorf aktuell kein Interesse hat
   dieses Thema weiter zu verfolgen."

Aufgrund der E-Mail von Rüdiger wird innerhalb der Mitgliederversammlung
darüber abgestimmt, ob das Team aufgelöst werden soll:

Dafür: 11  
Dagegen: 0  
Enthaltungen: 3

Das Team wird aufgelöst.

### Team Herzogenrath

- Budget aus Eigenmitteln + Förderungen
- Mitglieder: Volker Behrendt, Thomas Göttgens
- Bericht des Teams:
  - Bene im Auftrag

## 7.2 Geplante Änderungen in aktuellen Teams

Die Teams Freifunktag und Beggendorf werden aufgelöst, die anderen Teams
Backbone, Herzogenrath, Düren bleiben bestehen.

## 7.3 Vorgeschlagene, neue Teams

Es werden keine weiteren Teams vorgeschlagen.


# [TOP 8] Geplante Aktivitäten

Folgende Aktivitäten des F3N sind für das laufende Geschäftsjahr (2020)
geplant:
- Beschaffung eigener Server-Hardware zur Sicherung der Netzstabilität
  - ggf. mit Hilfe von Fördergeldern
- Verhandlung mit einem weiteren Rechenzentrum zur Traffic-Durchleitung zur
  Optimierung von Datenflüssen
- Zusammenarbeit mit dem Welthaus vertiefen


# [TOP 9] Verschiedenes

Es wird über die Form von Online MVs diskutiert. 
Malte schlägt vor, dass man zu einer Offline MV auch nebenher eine Online MV
hinzuschalten kann. Die Möglichkeiten werden vom Vorstand geprüft.


# [TOP 10] Abschluss der Versammlung

Dennis Crakau bedankt sich für die Mitarbeit und verabschiedet sich bei allen
Anwesenden.

Ende der MV um 21:10 Uhr.

---
<br>
<br>
(Sarah Beschorner)<br>
Protokoll
<br>
<br>
<br>
(Dennis Crakau)<br>
Sitzungsleitung
<br>
<br>
<br>
(Malte Möller)<br>
Sitzungleitung Wahl 2. Vorsitzender

---
