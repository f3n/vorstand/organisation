Zeit: 11.9.2018, Start: 20:19 Uhr, Ende: 22:51 Uhr

Ort: Chico Mendes, Pontstraße 74-76, Aachen

Protokoll: Oliver Huberty (mit tatkräftiger Unterstützung aller Anwesenden im Pad)

---

__Anwesend__
- 14 Mitglieder persönlich, 2 Mitglieder in Vertretung
  - siehe Anwesenheitsliste

---

# [TOP 1] Begrüßung

# [TOP 2] Formalia

- Feststellung der Ordnungsmäßigkeit der Einladung zur Mitgliederversammlung
  - Keine Einwände durch die Anwesenden

- Feststellung Beschlussfähigkeit
  - Die MV ist Beschlussfähig

- Wahl Sitzungsleitung
  - Vorschlag Gregor Bransky
  - Vorschlag einstimmig angenommen

- Wahl Protokollführer
  - Vorschlag Oliver Huberty
  - Vorschlag einstimmig angenommen

- Art der Tagesordnung  
  a) Vorstandswahl am Anfang  
  b) Vorstandswahl am Ende
  - Es wurde einstimmig b) beschlossen
  
# [TOP 3] Bericht des Vorstands

- Bericht über 
  - Namensänderung
  - Notarbesuch
  - Finanzamt
  - Bankkonto
  - Modalitäten mit der Stadt
  - Migration des Mailservers
  - Besuch in Beggendorf
  - Präsentation des Vereins auf dem Ehrenwert
  - Anschaffung neue Flyer
  - Vorstand plant monatliches Treffen (2. Dienstag im Monat)
  - Einholung von Informationen für eine Vereinsversicherung bzw. Betriebsversicherung 

# [TOP 4] Bericht der Kassenprüfer

- Einnahmen
  - neben den Mitgliedsbeiträgen sind die 8000€ Förderung der Stadt Aachen mittlerweile auf dem Konto eingegangen 
- Ausgaben
  - Kontostand und darin enthaltene Zweckbindungen
  - Bericht der Kassenprüfer über vollständige Belege
  - Bestätigung der Kassenprüfer:
    - Es gibt keine Einwände gegen die Kassenprüfer
    - Bericht der Kassenprüfer wurde einstimmig angenommen

# [TOP 5] Entlastung des Vorstands

- Vorstand gesteht ein, den Kassenprüfbericht 3 Tage zu spät versandt zu haben
- Keine geheime Abstimmung durch ein Mitglied gewünscht
- Abstimmergebnis für Entlastung des Vorstandes
  - Für Entlastung: 8
  - keine Entlastung, aber Empfehlung für nächste MV: 1
  - Enthaltungen: 7
  - keine Entlastung: 0

# [TOP 6] Wahl des Vorstands

- Abstimmung über die Aussetzung der Wahl eines neuen Vorstandes da dieser erst die Hälfte seiner Amtszeit rum hat:
  - Enthaltungen: 3
  - Dafür: 13

# [TOP 7] Berufung von Teams als "Verein im Verein", welche Budget bekommen und einen Rechenschaftsbericht ablegen müssen

- Vorgeschlagene Teams
  - Team Düren
    - Budget aus Eigenmitteln
    - Markus Jakobs, Ralf Pütz, Emir,  Matthias Schmidt, 
  - Team Baesweiler (Beggendorf)
    - Budget aus Eigenmitteln + Förderungen
    - Rüdiger Emmler, Andreas Dorfer, Oliver Huberty, Gregor Bransky, Dennis Crakau 
  - Team Server / Backbone
    - Budget aus städtischen Mitteln: 4.000€ (2018), 10.000€ (2019)
    - Christian Bricart, Arne Bayer, Daniel Müllers, Benedikt Hoss, Jan Niehusmann, Malte Möller
  - Team Freifunktag
    - Budget aus Eigenmitteln und 2.500€
    - Benedikt Hoss, Gregor Bransky, Friederike Nitz

- Info von Jan: Wenn nicht anders besprochen, müssen Ausgaben im Rahmen des Budgetantrags nochmal durch eine Vorstandssitzung beschlossen werden

- Abstimmung über die Einrichtung der Teams wie oben angegeben
    - Enthaltung: 1
    - Zustimmung: 15

# [TOP 8] Abstimmung über Geschäfts-, Finanz- und Beitragsordnungen

- Frage des Vorstands an die Versammlung zur Einholung eines Stimmungsbilds zu Vorstandbeschlüssen bezogen auf
  - Möglichkeit den reduzierten Mitgliedsbeitrag auch Personen anzubieten, die schon in anderen "verwandten" Vereinen sind (z.B. FFRL; Auskunft darüber auf Vertrauensbasis)
  - Mögliche Grenze der finanziellen Unterstützung um auf Webseite und Werbemitteln (Plakate) genannt zu werden

- Abstimmung über die Beitragsordnung:
  - Zustimmung: 15 
  - Enthaltung: 1
  - Dagegen: 0

- Stimmungsbild: Firmen als Paten für reduzierte Mitglieder als Leitlinie für den Vorstand
- Stimmungsbild: Minimalbeitrag für öffentliche Nennung
- Stimmungsbild: Verminderte Beiträge für Mitglieder in anderen Vereinen

- Abstimmung über die Änderungen an der zuvor bereitgestellten Finanzordnungsvorlage
  - Finanzordnung Punkt "Ausgaben 2.1" statt Ausgaben in Höhe von 20€, dürfen die Vorstandsmitglieder bis 50€ ohne Zustimmung der anderen Vorstandmitglieder selbstständig entscheiden 
  - Abstimmungsergebnis
    - Zustimmung: 12
    - Enthaltung: 4
    - Dagegen: 0

- Abstimmung über die zweite Änderung an der Finanzordnungsvorlage:
  - Finanzordnung Punkt "Ausgaben 2.3." Ausgaben >500€ sollen mit max einer Gegenstimme bei qualifizierter Mehrheit beschlossen werden können
  - Abstimmungsergebnis
    - Zustimmung: 15
    - Enthaltung:  1
    - Dagegen: 0

- Abstimmung zur Annahme der geänderten Finanzordnung:
  - Zustimmung: 15
  - Enthaltung: 1
  - Dagegen: 0

# [TOP 9] anzustrebende Mitgliedschaften des Vereins

*Info Jan: Die Abstimmungen dienen als Meinungsbild*

- Abstimmung über anzustrebende Mitgliedschaften des Vereins:
  - Mitgliedschaft im digitalHUB Aachen e.V.
    - Zustimmung: 16 
    - Enthaltung: 0 
    - Dagegen: 0
  - *Info GB: Supporter-Breitrag beträgt 250€ p.a., kann aber durch eine Fördermitgliedschaft des HUB beim F3N im Gegenzug ausgeglichen werden*

__Die MV wurde um 22:00 Uhr pausiert.__
__Die MV wurde um 22:15 Uhr fortgesetzt.__

  - Akademische Vereinigung RWTH
    - Zustimmung: 13
    - Enthaltung: 3 
    - Dagegen: 0

    - *Info GB: keine finanzielle Verpflichtung*

# [TOP 10] Förderung der Teilnahme von Mitgliedern an Veranstaltungen

- Abstimmung: Darf der Vorstand die Möglichkeit zur finanziellen Unterstützung von Personen, die an relevanten Veranstaltungen teilnehmen,
  in Betracht ziehen (z.B. Erstattung Fahrtkosten/Unterkunft/Verpflegung)
  - Zustimmung: 12
  - Enthaltung: 4
  - Dagegen: 0

# [TOP 11] Geplante Aktivitäten

  - Freifunktag in Aachen
    - Es werden noch Referenten gesucht
  - Förderanträge
  - Anschaffung von Servern für den Betrieb von Freifunk Aachen
  - Richtfunkstrecke für Beggendorf
  - Glühweintrinken am 04.12. geplant 

# [TOP 12] Richtlinien zur Annahme von Fördermitgliedschaften

- Weisung an den Vorstand
  - Der Vorstand entscheidet über die Annahme von Fördermitgliedern. Jedes Vorstandsmitglied hat ein Vetorecht.
  - Sollte von diesem Gebrauch gemacht werden, wird die Entscheidung auf die nächste MV vertragt und durch Abstimmung der Mitglieder entschieden.
  - Abstimmergebnis der Weisung
    - Zustimmung: 15 
    - Enthaltung: 1
    - Dagegen: 0

# [TOP 13] Verschiedenes

- Die Verteiler von "allgemeinen" E-Mail-Adressen benötigen eine Überarbeitung
  - kontakt@freifunk-aachen.de - Verteiler
  - technik@freifunk-aachen.de - Verteiler
  - Die Überarbeitung wurde auf das Community-Treffen verlegt

# [TOP 14] Abschluss der Versammlung

Ende der MV um 22:51 Uhr.
