=============
Finanzordnung
=============

Die Finanz- und Beitragsordnung wurde am 11. September 2018 von der Mitgliederversammlung beschlossen.

Grundsätze
==========

    1. Grundlagen dieser Finanzordnung sind
        1. Die Satzung des Vereins
        2. Die Beschlüsse der Mitgliederversammlung
    2. Mittel des Vereins dürfen nur für im Sinne der Satzung und dieser Finanzordnung verwendet werden.
    3. Haushaltsjahr ist das Kalenderjahr
        1. Das Kalenderjahr 2018 ist ein Rumpfgeschäftsjahr und beginnt mit der Gründung.

Verantwortlichkeiten
====================

    1. Verantwortlich für die finanzielle Tätigkeit des Vereins ist der Kassenwart.
    2. Berichterstattung
        1. Im Rahmen der Vorstandssitzungen erstattet der Kassenwart Bericht über die aktuelle finanzielle Situation des Vereins.
        2. Der Finanzbericht ist zur ersten Mitgliederversammlung im Folgejahr durch den Kassenwart vorzulegen.
    3. Jeder, der im Namen des Vereins Gelder einnimmt oder ausgibt, hat dies ordentlich zu dokumentieren. Hierzu gehören:
        - Datum
        - Art der Einnahme/Ausgabe
        - Betrag
    4. Auslagen werden nur gegen Einreichung von Belegen erstattet.

Einnahmen
=========

Mitgliedsbeiträge
-----------------

    1. Der Verein erhebt Mitgliedsbeiträge entsprechend der Beitragsordnung.

Einnahmen im Rahmen von Veranstaltungen
---------------------------------------

    1. Einnahmen im Rahmen von Veranstaltungen sind gemäß der Finanzordnung zu dokumentieren.

Ausgaben
========

    1. Zulässig sind:
        1. Ausgaben im Sinne der Satzung
        2. Kosten der laufenden Geschäftstätigkeit (z.B. Gebühren, Porto, Büromaterial, Postfach, Geschäftsstelle, Telefonkosten)
        3. Gestaltung von Mitgliederversammlungen
        4. Auslagen im Rahmen von Vorstandssitzungen und Veranstaltungen
            1. Speisen und Getränke im angemessenen Rahmen
            2. Fahrtkosten im angemessenen Rahmen
    2. Bis zu einer Höhe von
        1. 50 € ist jedes Vorstandsmitglied einzeln entscheidungsberechtigt
        2. zwischen 20,01 € und 499,99 € ist der Vorstand mit einfacher Mehrheit entscheidungsberechtigt
        3. ab 500 € darf hierbei die Entscheidung des Vorstands mit höchstens einer Gegenstimme erfolgen
        4. Bei Entscheidungen über die Förderung von Vereinsmitgliedern haben die Nutznießer kein Stimmrecht.
    3. Diese Festlegung gilt nur für die Beschlussfassung im Innenverhältnis. Die Handlungsbefugnis des Vereins im Außenverhältnis, insbesondere die Verfügung für Vereinskonten, ist davon nicht betroffen.

Inkrafttreten und Geltungsdauer
===============================

    1. Diese Finanzordnung gilt zeitlich unbegrenzt und kann nur durch die Mitgliederversammlung geändert werden.
    2. Redaktionelle Änderungen sind hiervon nicht betroffen.


Lizenz
======

Lizenz: CC BY-SA 4.0
Quelle: https://www.freifunk-rheinland.net/der-verein/ordnungen/ (Inhaltlich geändert)

