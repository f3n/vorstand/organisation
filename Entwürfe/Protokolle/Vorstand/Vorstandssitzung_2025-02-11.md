Zeit: 11.02.2025, Start: 19:10 Uhr, Ende: 19:50 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

**Anwesend**

- Jan
- Arne
- Malte
- Felix
- Josha
- Stefan
- Dennis

**Abwesend**

- Florian

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 04.02.2025 per Mail.
- Die Sitzung ist beschlussfähig


# [TOP 1] Annahme von Protokollen

Das Protokoll der Vorstandssitzung vom 14.01.2025 wurde einstimmig mit einer
Enthaltung angenommen.


# [TOP 2] Stand städtische Förderung

- Josha würde am Termin mit der Stadt teilnehmen wollen
- Arne klärt den Termin


# [TOP 3] MV2025

- Termin 26.03.2025 19:00 Uhr
- geplanter Veranstaltungsort: Hotel "Zur Brücke" in Herzogenrath
  - https://www.zur-bruecke.com/de/
- Felix
  - fragt bei den Kassenprüfern nach, ob diese am Termin können
  - klärt, ob der Veranstaltungsort dann verfügbar ist


# [TOP 4] Aachen zeigt Engagement

- dieses Top wurde übersprungen


# [TOP 5] Verschiedenes und Termine

- keine Punkte zu diesem Top
