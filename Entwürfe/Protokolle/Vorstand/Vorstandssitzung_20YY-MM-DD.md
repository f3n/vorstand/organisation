Zeit: xx.xx.20xx, Start: xx:xx Uhr, Ende: xx:xx Uhr

Ort: Gebäude, Ort

Protokoll: Vorname Nachname / Pseudonym

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---

# [TOP n] Formalia

- Vorstand (un)vollständig anwesend
- Einladung erfolgte ordnungsgemäß am xx.xx.20xx bei der Vorstandssitzung.
ODER
- Einladung erfolgte ordnungsgemäß am xx.xx.20xx bei persönlichem Treffen bzw. 
  per persönlicher telefonischer Rückfrage. Alle Vorstandsmitglieder sind mit der
  verkürzten Einladungsfrist einverstanden.

# [TOP n+1] Foo

- foo
  - bar
- foobar
