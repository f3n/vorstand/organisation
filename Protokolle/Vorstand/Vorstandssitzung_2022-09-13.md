Zeit: 13.09.2022, Start: 19:40 Uhr, Ende: 19:45 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis

__Abwesend__

---

# [TOP 0] Formalia
- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 06.09.2022 per E-Mail

# [TOP 1] Annahme des Protokolls vom 16.08.2022
- einstimmig angenommen

# [TOP 2] PSD Bank Vereinspreis
- es soll kein besonderer Aufwand für Werbung betrieben werden

# [TOP 3] Verschiedenes und Termine
- Rechenschaftsbericht wird heute von Sarah versendet
