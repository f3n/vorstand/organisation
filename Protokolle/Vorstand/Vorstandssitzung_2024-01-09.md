Zeit: 09.01.2024, Start: 19:35 Uhr, Ende: 20:15 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Dennis
- Stefan
- Jan
- Matthias
- Florian

__Abwesend__
- Malte

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 02.01.2024 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 14.11.2023
  - Einstimming angenommen

# [TOP 2] Stand MV Nachbereitung

- Kontozugriff Matthias & Arne
  - Matthias muss noch Daten zukommen lassen
  - Danach macht Jan einen Banktermin aus
    - kann evtl Remote durchgeführt werden

# [TOP 3] Richtfunk

- Förderung
  - aktuell keine in Aussicht (aus Landes- und Bundesebene)
  - man kann noch mal bei den bekannten Stellen nachfragen
  - ohne Fördertopf eher uninteressant für uns
  - man sollte mal die Kosten (vor allem laufende/wiederkehrende Kosten)
    kalkulieren
- Erwerb einer Lizenz für die Nutzung von BFWA Frequenzen im 5GHz Band von der
  Bundesnetzagentur für den Kreis Aachen
  - bisher nichts passiert

# [TOP 4] Supernodes Hardware

- SSDs teils etwas mitgenommen
  - Hälfte der Lebenszeit wurde schon erreicht
  - die verbauten SSDs sind von Ende 2020
  - Arne holt Angebote rein
  - 1TB ab ~100€

# [TOP 5] Verschiedenes und Termine

- offene Mitgliedsbeiträge
  - Florian hat viele offene Forderungen abgeklappert
  - es kamen seit dem viele offene Zahlungen rein
  - nächste Kontrolle vor der nächsten MV sinnvoll

- Fördergeld Stadt Aachen
  - Alternativangebot Server-Housing einholen
    - Malte war nicht da zum fragen

- Kaufland Spendenprogramm
  - hat zeitlich nicht gepasst

- gitlab
  - ist erledigt

- Anpassungen Homepage freifunk-aachen.de
  - Impressum
    - ist erledigt
  - Als Spendenadresse wurde zusätzlich die F3N-Bankverbindung angegeben

- Kontaktinformationen freifunk-aachen.de Domain
  - Jan überprüft
