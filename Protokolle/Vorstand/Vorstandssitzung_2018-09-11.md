Zeit: 11.9.2018, Start: 20:00 Uhr, Ende: 20:15 Uhr

Ort: Chico, Aachen

Protokoll: Dennis Crakau

---

__Anwesend__
- Jan
- Arne
- Malte
- Gregor
- Dennis

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 31.8.2018 bei der Vorstandssitzung.

# [TOP 2] Beitrittsantrag von RP inkl Beitragsminderung

- wurde einstimmig beschlossen
