Zeit: 12.11.2019, Start: 19:45 Uhr, Ende: 21:15 Uhr

Ort: Labyrinth, Aachen

Protokoll: Malte
Sitzungsleitung: Sarah

---

__Anwesend__
* Dennis
* Sarah
* Jan
* Arne
* Matthias
* Malte

---

# [TOP 1] Formalia

* Vorstand vollständig anwesend
* Einladung erfolgte ordnungsgemäß am 05.11.2019 per E-Mail.

## Protokoll 08.10.2019

* Gemeinsame Anpassung Protokoll 08.10.2019
* Einstimmig mit einer Enthaltung angenommen

## Protokoll 10.09.2019

* Einstimmig mit einer Enthaltung angenommen

# [Top 2] Rechenschaftsbericht 1.Halbjahr 2019

* Sarah hat einen Bericht geschrieben, Arne und Malte haben bereits Anpassungen per Git vorgenommen.
* Sarah erarbeitet die naechste Fassung.

# [Top 3] Aufsetzen des Ticketsystems

* Ticketsystem ist verfuegbar unter: http://ffac-ticket.skydisc.net

* crypto kommt, dann accounts.

* Folgendes wollen wir einbinden:
    * E-Mails
    * kontakt@
    * technik@
    * Forum per Mailabo
* Facebook
    * Klaerung API Tokens
* Twitter

# [Top 4] Kontakt zu NetAachen und RelAix

* Jan greift das alte Angebot weiteren Infos auf und bittet per Mail um aktualisiertes Angebot. Mailformulierung durchgesprochen und einstimmig angenommen, geht so raus.

* Bei NetAachen AG muessen wir auch eine Haftpflich abschliessen um Datacenter Zugang zu erhalten.

# [Top 5] Linux Presentation Day - Organisation

* Eroeffnung für Gaeste um 12 Uhr

* Paul kuemmert sich um den Vortrag (15:30), Malte unterstuetzt im Fragenteil.

* Beim Router Flash Workshop sind dabei: Sarah, Dennis, Jan, Felix, Malte 

* Malte bringt Beachflag, Flyer, Banner, Router und Anschlussmaterial mit.

# [Top 6] Hardwareanschaffung

* Können wir auf Landesmittel gebrauchte Hardware anschaffen?

* etwa 3000€ für neue System
* etwa 1300€ für recycelte Systeme

# [Top 7] Verschiedenes und Termine

* Erinnerung an Gluehweintreffen, bitte eintragen
* Rechnung Digihub ist eingegangen, Jan kuemmert sich. Die Rueckmitgliedschaft des Digihub, die uns laut Gregor zugesagt wurde, gibt es noch nicht. Sarah und Malte sind demnaechst dort und fragen nach.
* Steuer 2018: Koerperschaftsteuer ist durch mit festgestelltem Verlust von 3.118EUR. Das Geld der Stadt gilt nicht als Einkommen, sondern als Foerdergeld. Es wurde noch eine Gewerbesteuer Erklaerung angefragt, dies koennte aber auch ein Missverstaendnis sein. Jan klaert dies telefonisch mit dem Finanzamt. Sofern wirklich notwendig, beauftragt er die Steuerberaterin.
* Was ist unser Ziel als Verein, wie beziehen wir die Breite der Mitglieder mit ein.
    * Vorschlag zu Themenabenden, Verein koennte den Rahmen schaffen, Bespielsweise indem er einen Raum zur Verfuegung stellt.
    * Anfrage an Betreiber von Bemerkenswerten Installation, Aachener Bank, Skydisc, EWV, etc, ob sie nicht einen Beitrag fuer unsere Homepage machen moechten in dem die jeweilige Installation vorgestellt wird.
    * Themen vorstellen beim Communitytreffen, ggf kleine Vortraege
    * Aachener Bank laedt uns zur Eroeffnung der Geschaftsstelle Richterrich ein und moechte uns in diesem Rahmen mit 500EUR foerdern. Bedingung ist Gemeinnuetztigkeit, daher koennten wir den FFRL mit diesen 500EUR foerdern lassen. Einstimming dafuer.
    * Homepage: Betterplace iframe ersetzten durch Grafik um die google analytics Dinge von Betterplace loszuwerden. Sarah kuemmert sich.
* naechste Vorstandssitzung vorraussichtlich am 10.12.2019
