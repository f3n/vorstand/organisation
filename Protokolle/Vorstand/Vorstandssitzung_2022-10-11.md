Zeit: 11.10.2022, Start: 19:35 Uhr, Ende: 19:40 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Malte
- Sarah
- Dennis

__Abwesend__
- Arne

---

# [TOP 0] Formalia
- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 04.10.2022 per E-Mail

# [TOP 1] Annahme des Protokolls vom 13.09.2022
- einstimmig angenommen

# [TOP 2] PSD Bank Vereinspreis
- kein Preisgeld gewonnen
- Positive Aspekte
  - F3N wurde in der Öffentlichkeit wahrgenommen
  - Posts in Social Media Kanälen

# [TOP 3] Rückmeldung Rechenschaftsbericht
- Sarah antwortet auf die Rückmeldung, in der weiterhin Rechenschaftsberichte
  eingefordert werden, damit die zweckgebundenen Ausgaben weiterhin
  dokumentiert werden

# [TOP 4] Verschiedenes und Termine
- nächste Vorstandssitzung 18.11.2022
- Jan spricht mit dem Steuerberater bzgl. Steuererklärung
