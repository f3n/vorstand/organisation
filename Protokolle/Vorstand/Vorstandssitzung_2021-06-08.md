Zeit: 08.06.2021, Start: 19:35 Uhr, Ende: 20:11 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)


Protokoll: Dennis

---

__Anwesend__
- Arne
- Malte
- Sarah
- Dennis
- Matthias

__Abwesend__
- Jan

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 01.06.2018 bei der Vorstandssitzung.


# [TOP 2] Annahme des Protokolls vom 11.05.2021

- Einstimmig angenommen


# [TOP 3] Mitgliedsantrag

- Einstimmig angenommen


# [TOP 4] Rechenschaftsbericht Stadt Aachen

- Bisher noch nicht angefangen


# [TOP 5] Mitgliedervollversammlung 2021 Protokoll & Notar

- Brief ist mit dem unterschriebenen Protokoll ist heute bei Sarah angekommen.
- Sarah und Jan machen einen Termin mit dem Notar aus um die Änderungen
  durchführen zu lassen.


# [TOP 6] Antrag Welthaus

- Gestern wurde nochmal darüber gesprochen
- Gestern wurde eine neue Version (V8) versendet
  - muss noch durchgeschaut + bewertet werden
- es soll eine weitere Besprechung mit allen Beteiligten stattfinden
  - Termin muss noch gesucht werden


# [TOP 7] Verschiedenes und Termine

- Sarah und Malte ziehen um, wodurch "Freifunk-Sachen" nicht mehr gelagert
  werden können. Es wird ein neues Zuhause dafür gesucht.
  - Dinge wie bspw. Beachflag, Beschwerung Zelt etc ...
  - Matthias hat evtl. Platz dafür
