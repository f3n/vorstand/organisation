Zeit: 10.05.2022, Start: 19:41 Uhr, Ende: 19:53 Uhr

Ort: Labyrinth, Aachen

Protokoll: Dennis

---

__Anwesend__
- Jan
- Sarah
- Dennis

__Abwesend__
  Arne
- Malte
- Matthias

---


# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Vorstand ist beschlussfähig
- Einladung erfolgte ordnungsgemäß am 07.06.2022 per E-Mail


# [TOP 2] Annahme Protokoll vom 10.05.2022

- Protokoll-Annahme wurde auf die nächste Vorstandssitzung vertagt


# [TOP 3] Mitgliedervollversammlung 2022 - Nachbereitung

- es gab weder im Voraus, noch beim Treffen Einwände gegen das Protokoll
- das ausgedruckte Protokoll wurde von Sarah, Dennis und Jan unterschrieben
- Jan legt das unterschriebene Protokoll in den Vereins-Ordner


# [TOP 4] Verschiedenes und Termine

- Sommerpause Vorstandssitzungen
  - für die Vorstandssitzungen wurde eine Sommerpause mit offenen Ende beschlossen
  - die Sommerpause wird mir der gewohnten Einladung per E-Mail beendet
  - außerordentliche Vorstandssitzungen können per E-Mail/Slack beschlossen werden
  - die Community-Treffen finden weiterhin statt
- die Community meldet den F3N bei der Aldi-Spendenaktion an
