Zeit: 13.10.2020, Start: 19:40 Uhr, Ende: 20:05 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Arne

---
Anwesend:
- Arne
- Sarah
- Malte
- Dennis
- Matthias
- Jan

# [TOP 1] Formalia

- Einladung erfolgte ordnungsgemäß per Mail durch Sarah am 06.10.2020


# [TOP 2] Annahme des Protokolls vom 08.09.2020

- vertagt, da das Protokoll nicht vorliegt


# [TOP 3] Rechenschaftsbericht 1. Halbjahr 2020

- soweit fertig
- es fehlen noch Zahlen
- bitte alle nochmal lesen


# [TOP 4] Zweites Rechenzentrum ComNet

- Hardware steht noch aus, daher noch nichts neues


# [TOP 5] Verschiedenes und Termine

- Punkt entfallen
