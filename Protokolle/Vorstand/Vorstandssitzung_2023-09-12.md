Zeit: 12.09.2023, Start: 19:40 Uhr, Ende: 20:30 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Jan Niehusmann

---

__Anwesend__
- Matthias
- Florian
- Jan
- Malte
- Stefan

__Abwesend__
- Arne
- Dennis

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend, beschlussfähig
- Einladung erfolgte ordnungsgemäß am 01.09.2023 per Mail.

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 08.08.2023
  - Einstimmig mit einer Enthaltung angenommen

# [TOP 2] Stand MV Nachbereitung

- Matthias und Arne sind im Vereinsregister eingetragen
- Sobald die Eintragung vorliegt, sollen Matthias und Arne als Kontobevollmächtige eingetragen werden
  - Jan soll wenn möglich weiter Lesezugriff auf das Konto haben

# [TOP 3] Rechenschaftsbericht H1/2023

- Feedback bitte bis spätestens kommenden Dienstag, danach schickt Arne das raus

# [TOP 4] Verschiedenes und Termine

- Aktuellen Vorstand als Owner in Github / Gitlab eintragen
  - Malte kümmert sich darum
  - Im Infra-Git dokumentieren

- Mitgliedsantrag vom 9.8.2023
  - Einstimmig angenommen

- Richtfunk
  - Stefan erwähnt Installationen in anderen Städten und möchte auch in Aachen Richtfunkstrecken etablieren
  - Eventuell Türme -> Drehturm, von da weiter?
    - Optionen werden diskutiert, Details was am Drehturm möglich ist, muss aber noch geklärt werden
    - Testinstallation im Seilgraben bei Stefan wäre sinnvoll
    - Stefan, Malte und Marius versuchen einen Termin zu finden, an dem sie das Dach des Drehturms besichtigen können
  - Finanzierungsmöglichkeiten werden diskutiert
    - Landesförderung soll geprüft werde

- Felix berichtet von Plänen ein Ticketsystem aufzusetzen
  - Soll an Freifunk-Mails angebunden werden
  - Marius soll Adminzugriff auf den Mailcow bei Relaix bekommen

