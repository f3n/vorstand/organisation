Zeit: 10.05.2022, Start: 19:41 Uhr, Ende: 19:53 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Sarah

---

__Anwesend__
- Jan
- Malte
- Sarah
- Matthias
  Arne

__Abwesend__
- Dennis

---


# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Vorstand ist beschlussfähig
- Einladung erfolgte ordnungsgemäß am 03.05.2022 per E-Mail


# [TOP 2] Annahme Protokoll vom 08.04.2022

- einstimmig angenommen


# [TOP 3] Mitgliedervollversammlung 2022

- Einladung wurde von Jan am 01.05.2022 rausgeschickt
- Unterlagen zur Kassenprüfung wurden von Jan rausgeschickt
- Präsentation wird von Dennis vorbereitet
- Kassenbericht wird sobald dieser geprüft ist an die Mitglieder gesendet. Jan wird sich darum kümmern. 
- Teams sollen eine kurze Zusamenfassung an der MV präsentieren
  - Team Düren : Matthias
  - Team Technik: Shiva
  - Team Welthaus: Malte


# [TOP 4] Verschiedenes und Termine

- Anmeldung zum Ehrenwert ist bisher immer noch nicht möglich, Sarah wird dies weiterhin beobachten
- 21. August 2022 soll die MV Freifunk Rheinland stattfinden, Matthias lädt hierzu herzlich nach Düren ein
