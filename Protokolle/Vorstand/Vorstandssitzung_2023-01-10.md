Zeit: 10.11.2023, Start: 19:40 Uhr, Ende: 19:55 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Dennis

__Abwesend__
- Sarah
- Malte

---

# [TOP 0] Formalia
- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 06.12.2022 per E-Mail

# [TOP 1] Annahme des Protokolls vom 13.12.2022
- Eine Enthaltung

# [TOP 2] Rechenschaftsbericht 02/2022
- Sarah nicht anwesend
- Arne
  - Uplink etwas teurer geworden
  - die Systeme wurden weitgehenst auf Proxmox umgestellt
  - stellt Sarah Text zur bzgl. was im Hintergrund passiert ist zur Verfügung
  - stellt Sarah eine Traffic-Grafik zur Verfügung

# [TOP 3]  Verschiedenes und Termine
- Geld für neue Freifunk-Sticker
  - Sarah hat noch keine Rücksprache mit Jan gehalten
  - ungefährer Preisrahmen ist unbekannt
    - laut schneller Internet-Recherche kosten 1000 Sticker ca 35€
  - größere Stückzahl ist sinnvoll, weil die Preise dann erfahrungsgemäß
    schnell günstiger werden
  - im Vorstand wurden einstimmig formell 150€ für den Einkauf freigegeben
    - wenn das Geld nicht ausreichen sollte, dann bitte Rücksprache mit Jan
      halten
- Matthias wieder in Vorstand aufnehmen
  - Unabhägig davon, ob Matthias offiziell ausgetreten war, wurde Matthias
    wieder einstimmig aufgenommen
  - bei nächster Vorstandssitzung im Vorstand darüber abstimmen, ob wir
    Matthias wieder im Vorstand aufnehmen wollen.
    - sollte lt. Satzung möglich sein
- Steuererklärung liegt bei Steuerberaterin
