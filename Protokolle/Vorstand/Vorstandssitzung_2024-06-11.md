Zeit: 11.06.2024, Start: 19:55 Uhr, Ende: 20:03 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Arne

---

__Anwesend__
- Jan
- Arne
- Felix
- Stefan
- Josha

__Abwesend__
- Malte
- Florian
- Dennis

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 04.06.2024 per Mail
- Sitzung wird auf ein Minimum reduziert wegen Gloun-Meeting

# [TOP 1] Aachen zeigt Engagement

- Planungstreffen am 15.6., weiteres im Slack
- eventuell Schichten für den Tag planen
- Bierzeltgarnitur sollte noch organisiert werden

