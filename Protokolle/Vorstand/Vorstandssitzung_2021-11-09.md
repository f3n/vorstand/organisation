Zeit: 09.11.2021, Start: 19:55 Uhr, Ende: 20:35 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

__Entschuldigt__

__Fehlt__

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 02.11.2021 per E-Mail.


# [TOP 2] Annahme des Protokolls vom 12.10.2021

- Es wurde ein Satz aus Top3 entfernt
- mit zwei Enthaltungen angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen- Feedback vom Vorstand

- Zahlen von Jan sind noch aktuell
- Traffic-Graphen wurden an Sarah geschickt und müssen noch bewertet werden
- Protokoll soll am 12.11.2021 rausgeschickt werden
  - bis dahin können Rückmeldungen an Sarah eingehen


# [TOP 4] Förderantrag Welthaus

- Das Projekt Welthaus ist bis auf Weiteres pausiert


# [TOP 5] Verschiedenes und Termine

-  Weihnachtsfeier
  - aufgrund der atuellen Corona-Situation ist derzeit keine Weihnachtsfeier
    geplant
-  Gemeinützigkeit
  - der F3N ist jetzt Gemeinützig
  - Mitgliedsbeiträge sind nicht absetzbar
- R3S NRW
  - Es steht die Frage im Raum in wiefern sich F3N daran beteiligen möchte/soll
  - Abstimmung erfolgt außerhalb der Vorstandssitzung
