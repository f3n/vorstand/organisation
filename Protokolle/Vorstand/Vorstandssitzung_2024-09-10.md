Zeit: 13.08.2024, Start: 19:20 Uhr, Ende: 20:10 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Josha
- Felix
- Dennis
- Stefan
- Florian

__Abwesend__

---

# [TOP 0] Formalia

- Vorstand vollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 03.09.2024 per Mail


# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 13.08.2024
  - mit 2 Enthaltungen angenommen


# [TOP 2] Nachbereitung MV

- Aachener Bank
  - Termin auf den 23.09.2024 verschoben


# [TOP 3] Rechenschaftsbericht H1/2024

- ist noch nicht im August rausgegangen
- soll im September rausgehen
  - am besten im Laufe der nächsten Woche


# [TOP 4] Nachweis Gemeinnützigkeit

- Arne hat einen Nachweis beim Finanzamt angefragt
  - aktuell keine Rückmeldung


# [TOP 5] Verschiedenes und Termine

- 13.+14.09.2024 https://allezhop-festival.de/
  - könnte mit Freifunk befunkt werden
- Amazon Alexa funktioniert nicht mehr im Freifunk-Netz
- Problem mit einem Netzwerkport auf dem Blech
  - vermutlich ein Defekt auf dem Mainboard
  - man kann über einen HW-Tausch (Neukauf) nachdenken oder alternativ
    eine PCIe-Karte einbauen, falls Platz vorhanden ist
