Zeit: 10.12.2019, Start: 19:30 Uhr, Ende: 20:16 Uhr

Ort: Labyrinth, Aachen

Protokoll: Sarah
Sitzungsleitung: Sarah

---
Anwesend:
__Anwesend__
* Dennis
* Sarah
* Arne
* Matthias

Entschuldigt:
* Malte
* Jan

---

# [TOP 1] Formalia

* Vorstand teilweise anwesend
* Einladung erfolgte ordnungsgemäß am 03.11.2019 per E-Mail.

## Protokoll 12.11.2019

* Abstimmung wird auf die nächste Vorstandssitzung im Jahr 2020 vertagt.

# [Top 2] Rechenschaftsbericht 1.Halbjahr 2019

* Sarah hat den fertigen Bericht am 01.12.2019 an die Stadt geschickt. Es wurde ein neuer Ansprechpartner festgelegt.
* Solange es keine Anmerkungen bezüglich der Stadt gibt, ist der Tagungspunkt abgeschlossen.

* Verantwortlich: Sarah 

# [Top 3] Aufsetzen des Ticketsystems

* Ticketsystem ist verfuegbar unter: http://ffac-ticket.skydisc.net

* Accounts für die Vorstandsmitglieder wurden von Arne anglegt.
* Das Ticketsystem soll von allen Freifunkmitgliedern Aachens genutzt werden. Hierfür soll Werbung gemacht werden
* Das Ticketsystem soll mit aktuellen Projekten gefüllt werden.

* Folgendes wollen wir einbinden:
* Facebook
    * Klaerung API Tokens
* Twitter

* Verantwortlich: Arne & Sarah

# [Top 4] Kontakt zu NetAachen und RelAix

* Keine neuen Informationen, da Jan nicht anwesend.
* RelAix soll sich im Januar melden

* Verantwortlich: Jan

# [Top 5] Ünterstützung des Open Infrastructure Orbits

* Der Orbit bittet um Spenden, jedoch kann eine Rechnungstellung erst im Jahr 2020 erfolgen.
* Eine direkte Unterstützung über Spendengelder kann nur über eigene Mittel erfolgen. 
* Es wurde auf der aktuellen Informationsbasis noch kein entgültiger Beschluss getroffen. 

* Verantwortlich: Vorstand
 
# [Top 6] Förderantrag NRW für Server

* wird Anfang Januar fertiggestellt und vorgelegt

* Verantwortlich: Dennis

# [Top 7] Ünterstützung des FFRL Auszahlung
* FFRL soll mit 50€ im Monat unterstützt werden, da der Freifunk Aachen hier viel des Backbones mitbenutzt.
* Die Zahlung für das Jahr 2019 sollte abgerechnet worden sein. Nachfragen bei Jan. 
* Es wurde beschlossen, dass der Freifunk Aachen sich mit einer Endjahresabsschlusszahlung von 500-1000€ aufgrund des steigenden Traffics beteiligen möchte.
* einstimmig beschlossen (4 Stimmen)

* Verantwortlich: Jan

# [Top 8] Verschiedenes und Termine
* Matthias ist für das Vereinsjahr 2020 Kassenprüfer beim FFRL
* Eintragen in die Doodle-Umfrage für die Planung der Mitgliederversammlung. Deadline: 20.12.2019
