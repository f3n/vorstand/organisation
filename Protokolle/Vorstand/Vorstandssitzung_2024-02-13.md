Zeit: 13.02.2024, Start: 19:40 Uhr, Ende: 20:20 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Malte
- Dennis
- Florian
- Matthias

__Abwesend__
- Jan
- Stefan

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 06.02.2024 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 09.01.2024
  - Einstimmig angenommen

# [TOP 2] Stand MV Nachbereitung

- Aachener Bank Termin ist erfolgt
  - Jan hat Lesezugriff
  - Arne und Matthias haben beide Vollzugriff

# [TOP 3] Vorbereitung MV 2024

- Vorschlag
  - Mittwoch, den 10.04.2024 19:00Uhr bei RelAix
- Remote-Teilnahme (View) soll ermöglicht werden

- Rechenschaftsbericht H2/2023
  - sollte vor der MV an die Stadt Aachen versendet werden

- Alternativangebote Server-Housing einholen
  - sollte vor der MV eingeholt werden
  - Malte kümmert sich drum

# [TOP 4] Richtfunk

- keine Neuerung

# [TOP 5] Supernodes Hardware

- Angebot SSDs liegt vor
  - Angebot wird nochmal aktualisiert und dann per E-Mail-Zustimmung bestellt

# [TOP 6] Verschiedenes und Termine

- Mitgliedsantrag vom 12.01.2024
  - Einstimmig dafür

- Kontaktinformationen freifunk-aachen.de Domain
  - siehe Protokoll vom 14.11.2023
  - aktuelle Adresse Hostmaster@freifunk-aachen.de bleibt
    - die E-Mail-Adresse generiert ein Ticket
