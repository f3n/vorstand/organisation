Zeit: 12.05.2020, Start: 19:45 Uhr, Ende: 20:19 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---
Anwesend:
- Jan
- Arne
- Malte
- Dennis
- Sarah
- Matthias (ab 20:05)

Abwesend:
- xxx
---


# [TOP 1] Formalia

- Vorstand war bis 20:05 unvollständig und anschließend vollständig anwesend
- Einladung erfolgte ordnungsgemäß per Mail durch Sarah am 05.05.2020


# [TOP 2] Annahme der Protokolle vom 14.04.2020

- Einstimming angenommen


# [TOP 2.1] Info über Annahme alte Protokolle 2019-01-15 & 2019-03-12 im Umlaufverfahren

- muss von Jan und Dennis genehmigt werden
  - https://gitlab.com/f3n/vorstand/organisation/-/merge_requests/3


# [TOP 3] Rechenschaftsbericht 2.Halbjahr 2019

- wurde am 11.05.2020 von Sarah per E-Mail versendet
  - bisher keine Rückmeldung


# [TOP 4] Kontakt zu NetAachen, RelAix, ComNet

- Standort Rechenzentrum NetAachen verworfen. Es wird nun fokussiert eine Gig-Faser zu erhalten um a) das Netz zu verbessern und b) die Kosten zu optimieren
- Die Kommuniktation nach ComNet stockt aktuell


# [TOP 5] Förderantrag NRW für Server

- soll diese Woche von Dennis fertig gestellt werden


# [TOP 6] Verschiedenes und Termine

## Ehrenwert 30.08.2020    

- wurde per E-Mail am 11.05.2020 abgesagt


## Linux Presentation Day 16.05.2020, 14:00-18:00Uhr, Welthaus (online)

- wird als Videokonferenz stattfinden
- Anmeldung muss per Anmeldung@Akademie-der-Vereine.de erfolgen, damit man weitere Informationen erhält.
