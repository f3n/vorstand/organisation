Zeit: 13.11.2018, Start: 21:00 Uhr, Ende: 22:20 Uhr

Ort: Labyrinth, Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- Jan
- Arne
- Malte
- Gregor
- Dennis

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte mit verkürzter Frist per EMail. Alle
  Vorstandsmitglieder sind mit der verkürzten Einladungsfrist
  einverstanden.

- Die Protokolle der folgenden Vorstandssitzung wurden angenommen
  und unterzeichnet:

  - Vorstandssitzung_2018-08-14.md
  - Vorstandssitzung_2018-08-31.md
  - Vorstandssitzung_2018-09-11.md
  - Vorstandssitzung_2018-10-16.md

# [TOP 2] Nachbesprechung Freifunktag

  - Es sind noch nicht alle Rechnungen eingegangen,
    daher noch keine endgültige Abrechnung möglich.
  - Kostenbeteiligung für Infobeamer: Details müssen noch geklärt werden, Gregor hakt nach

# [TOP 3] Internet-Traffic-Kosten Relaix

  - Relaix stellt wie bereits besprochen 300€/Monat für Server und Internet in Rechnung

# [TOP 4] Angebot NetAachen

  - Gregor verhandelt nach

# [TOP 5] Kostenbeteiligungen Kongress

  - Förderung für 1 Person konkret geplant, 2 weitere Kandidaten in Überlegung


