Zeit: 10.11.2020, Start: 19:44 Uhr, Ende: 20:31 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Jan

---
Anwesend:
- Sarah
- Matthias
- Malte
- Jan
- Dennis
- Arne

Abwesend:

# [TOP 0] Formalia

- Einladung erfolgte ordnungsgemäß per Mail durch Sarah am 03.11.2020

# [TOP 1] Annahme des Protokolls vom 08.09.2020 und 13.10.2020

- Protokoll vom 8.9. mit 5 Stimmen angenommen (Jan war nicht anwesend und enthält sich)
- Protokoll vom 13.10. liegt noch nicht vor -> vertagt auf nächste Sitzung

# [TOP 2] Mitgliedsanträge

Mitgliedsantrag des DGB einstimmig angenommen.

# [TOP 3] Rechenschaftsbericht 1.Halbjahr 2020

Ist soweit ok, Hinweis auf Annahme des NRW-Förderantrags soll noch
als Fußnote ergänzt werden. Bitte alle bis spätestens Freitag noch
einmal drüberlesen, damit Sahah das rausschicken kann.

# [TOP 4] Förderung NRW für Server

Zuwendungsbescheid liegt vor. Dennis und Sarah kümmern sich in den
nächsten Tagen um den Abruf der Fördergelder.

# [TOP 7] Einrichtung Server/ Bestellung

Server sind bestellt. Lieferant bittet um Vorauszahlung von 50%.
Daniel erklärt sich bereit ggf. Geld vorzustrecken, falls der Abruf
beim Land länger dauert.

Arne kümmert sich um Grundinstallation, anschließend werden die
VMs von den vorhandenen Servern migriert. Koordination übernimmt Malte.
Daniel übernimmt anschließend den Abbau in 'Charlie 5'.

# [TOP 8] Mitgliedschaft im DigitalHub

Wir kündigen die Mitgliedschaft zum nächstmöglichen Zeitpunkt.

# [TOP 9] Verschiedenes und Termine

Linux Presentation Day am 21.11.2020 mit Thema "barrierefreies Linux".
Passt thematisch nicht gut zu Freifunk, sodass wir uns dieses mal
nicht am Inhalt beteiligen werden. Wir helfen aber wieder mit den
Videokonferenz-Möglichkeiten, und (virtuelle) Anwesenheit wird natürlich
wie immer gerne gesehen.

