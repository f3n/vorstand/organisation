Zeit: 21.5.2019, Start: 20:20 Uhr, Ende: 21:38 Uhr

Ort: Labyrinth, Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- Arne (bis TOP 6)
- Dennis
- Jan
- Malte
- Matthias (bis TOP 5)
- Sarah

---

# [TOP 0] Formalia

- Vorstand vollständig anwesend.
- Einladung erfolgte ordnungsgemäß per Mail am 7.5.2019 durch Sarah.

# [TOP 1] Mitgliedsanträge

- Ein Mitgliedsantrag mit reduziertem Beitrag für 2019 wird einstimmig angenommen.

# [TOP 2] Ermäßigungen

- Die drei Anträge auf reduzierten Beitrag für 2019 werden einstimmig angenommen.

# [TOP 3] Protokoll 2019

- Protokoll ist fertig und liegt unterschrieben vor.

# [TOP 4] Eintragung Vereinsregister

- Unterlagen sind beim Notar, der nun die Eintragung vorbereitet.

# [TOP 5] Offene Aufgaben alter Vorstand
- Protokoll 2018 möchte Gregor noch einmal mit Oliver überarbeiten. Er kümmert sich darum.
- Rechenschaftsbericht 2018 Stadt Aachen muss zeitnah angefertigt werden. Er sollte enthalten:
  - Erklärung, wieso wir Geld nicht überwiegend für Serverbetrieb verwendet haben
  - Tätigkeitsbericht (wie bei MV vorgestellt)
  - Kassenbericht (auch über andere Mittel als die der Stadt)
  - Konkrete Verwendung der städtischen Mittel

  - Malte + Arne bereiten in den nächsten Tagen eine Rohfassung vor. Danach entscheiden
    wir, ob bzw. wann wir uns zur finalisierung treffen.

- Kostenbeteiligung für Infobeamer Freifunktag (Raspberry Pi)
  - Nachträgliche Finanzierung über Budget Freifunktag 2018 schwierig
  - Malte fragt an, ob wir uns statt dessen finanziell am Freifunktag 2019 beteiligen können / sollten

- Brander Markt
  - Infoveranstaltung sollte irgendwann durchgeführt werden, vorher Kommunikation mit Beteiligen
    um den aktuellen Stand überhaupt zu kennen. Sarah fragt nach.

- NetAachen
  - Planung weiteres Vorgehens per Chat bis zur nächsten Vorstandssitzung. Vermutlich muss
    als erstes der Kontakt freundlich wieder aufgebaut werden und erklärt werden, wieso wir
    auf das vorliegende Angebot bisher nicht reagiert haben.

[TOP 6] Reorganisation Vorstand
  - Kontovollmacht für Sarah sobald sie im Vereinsregister steht
  - Schlüssel vom DigitalHub: Jan schaut bei Gelegenheit im Hub vorbei und fragt, ob wir einen haben können

[TOP 7] Verschiedens und Termine
  - Wir schlagen Daniel vor, dass wir rückwirkend ab dem 1.1.2019 pro Monat 50€ als Kostenbeitrag
    für den Betrieb der zur Versorgung der Regio Aachen notwendigen Infrastrukur übernehmen können. Einstimmig beschlossen.

Nächste Vorstandssitzung voraussichtlich 18.6., 19:30

