Zeit: 12.11.2024, Start: 19:05 Uhr, Ende: 20:00 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Josha
- Felix
- Dennis
- Stefan
- Florian

__Abwesend__
- Malte

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 05.11.2024 per Mail


# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 08.10.2024
  - mit einer Enthaltung angenommen


# [TOP 2] Nachbereitung MV

- endlich ist alles geklärt/abgehakt
  - Arne & Josha haben jetzt einen Kontozugriff


# [TOP 3] Rechenschaftsbericht H1/2024

- Weiterverfolgung der städtischen Förderung
  - Arne steht in Kontakt mit der Stadt.
    - es soll nochmal der Ratsbeschluss von 2018, sowie die
      Rechenschaftsberichte der letzten 6 Jahre zugesendet werden


# [TOP 4] Bericht Ehrenamtstag Jülich

- Stefan hat berichtet
  - kleiner als der Tag in Aachen + Indoor, aber eine sehr angenehme
    Veranstaltung
    - dadurch resultierte eine 100% Ausleuchtung mit Freifunk
      - es wurde ein Router mit LTE verwendet
  - das Thema Freifunk ist aktuell noch in der Stadt Jülich verbrannt
  - es wurde nicht sehr viel Material mitgenommen


# [TOP 5] Verschiedenes und Termine

- Alexa funktioniert wieder im Freifunk-Netz

- Stefan und sein Vater haben sich mit der "Bezirksverwaltung Wald (Solingen)"
  Kontakt aufgenommen
  - wir erhalten eine Aufwandentschädigung i.H.v. 312€ für die Routerrettung

- Kleingartenverein Jülich
  - Begehung der Anlage durchgeführt, Randinformationen erhalten
  - es wurden zwei weitere Antennen aufgehangen, wodurch erstmal Zufriedenheit
    generiert wurde
  - anschließend wurde die Anlage ein zweites Mal besucht
    - der Gartenverein Eschenalee kam als Besuch mit um die Technik/Umsetzung
      präsentiert zu bekommen

- Gartenverein Eschenalee
  - hat eine Förderung zur Digitalisierung des Vereins erhalten
    - ein Teil der Förderung soll dazu verwendet werden Freifunk zu installieren
      und zu betreiben
  - hat eine Kaufliste angefragt, damit Geräte gekauft werden können

- Weihnachtsmarkt
  - es wurde ein Nuudle zur Terminfindung erstellt
