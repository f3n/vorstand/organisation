Zeit: 14.01.2025, Start: 19:15 Uhr, Ende: 20:25 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

**Anwesend**

- Arne
- Felix
- Jan
- Stefan
- Dennis

**Abwesend**

- Malte
- Josha
- Florian

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 07.01.2025 per Mail.
- Die Sitzung ist beschlussfähig


# [TOP 1] Annahme von Protokollen

Das Protokoll der Vorstandssitzung vom 10.12.2024 wurde einstimmig mit einer
Enthaltung angenommen.


# [TOP 2] Stand städtische Förderung

Arne berichtet über die Fördermöglichkeiten der Stadt Aachen.


# [TOP 3] MV2025

- Dennis hat ein Doodle zur Abstimmung des MV-Termins in Slack gepostet
  - https://nuudel.digitalcourage.de/f3n-mv2025
  - Abstimmung läuft bis zum 01.02.2025

- mögliche Termine
  - 01.04.2025 19:00Uhr
  - 03.04.2025 19:00Uhr
  - 08.04.2025 19:00Uhr

- Räumlichkeit klären
  - RelAix
  - Alternativen?


# [TOP 4] Verschiedenes und Termine

- Engangiert in Aachen
  - Termine
    - Anmeldefrist:  18.03.2025
    - Veranstaltung: 18.05.2025
  - dieses Jahr werden mehr Leute als beim letzten Mal benötigt
  - wollen wir eine bessere Freifunk-Installation?

- Wald (Solingen) hat uns am 30.12.2024 eine Spende zukommen lassen
