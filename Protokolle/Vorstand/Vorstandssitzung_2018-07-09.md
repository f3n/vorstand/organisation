Zeit: 9.7.2018, 19 Uhr

Ort: Labyrinth, Aachen

Protokoll: Jan Niehusmann


Einladung erfolgte ordnunungsgemäß am 3.7.2018 bei persönlichem Treffen bzw.
per persönlicher telefonischer Rückfrage. Alle Vorstandsmitglieder sind mit der
verkürzten Einladungsfrist einverstanden.

---

__Anwesend__
- Jan
- Arne
- Malte
- Gregor
- Dennis

---

# [TOP 1] Relaix-Traffic über F3N abrechnen

- Traffic der nicht durch Spenden gedeckt ist, kann vom F3N bezahlt werden
- Hierzu sollen bei Bedarf zunächst Angebote eingeholt werden
- Net-Aachen fragen, ob wir dort auch Server aufstellen können, was uns dabei
  Traffic kostet, ob Peering zu Relaix möglich ist.
- Jan schaut ob er Zuordnung Provider-Node-ID aus Fastd-Sockets generieren kann,
  Malte baut daraus Graphana-Filter um Traffic pro Provider ermitteln zu können
- Gregor kümmert sich um einen Termin mit NetAachen

# [TOP 2] Mitgliederversammlung 2018

- Wir sollten schauen, dass wir möglichst bald einen MV-Termin finden. Ziel sollte
  September sein (nach den Sommerferien)
- Terminvorschlag: 11.9.2018
- Mögliche Orte?
  - Chico Mendes
    - Malte fragt dort an ob der Termin geht
- 4.9. Vorbereitungstreffen für die MV (statt Community-Treffen)

# [TOP 3] Fördergelder von der Stadt

- Gregor hat Kontonummer an Stadt übermittelt
- Bisher (Stand heute) ist das Geld noch nicht auf dem Konto eingegangen
- Gregor stellt Ratsbeschluss, aus dem die Bedingungen der Förderung hervorgehen, ins Gitlab
- Geld, das wir nicht für TOP1 brauchen, nach Absprache mit der Stadt (Gregor) evtl. auch
  für Freifunktag verwenden

# [TOP 4] F3N-Domains

- Abstimmung, welche Domain gekauft werden soll, erfolgt per Mail.

# [TOP 5] Domain freifunk-aachen.de

- Webserver-Umzug kann / macht Malte
- Mail zu Relaix, wenn deren Angebot passt - Jan kümmert sich

# [TOP 6] Rolle des F3N bei der Organisation des Freifunktags festlegen

- Finanzierung nicht über Förderantrag (passt zeitlich nicht)
- F3N ist Veranstalter
- F3N schließt dafür Veranstaltungshaftpflicht ab (Gregor)

# [TOP 7] Ehrenwert

- 9.9.2018 (ca. 10-18 Uhr)
- Freifunk Aachen ist angemeldet
- Material des Vereins kann hier genutzt werden
  (Noch anzuschaffen.)
- Verantwortlich: Gregor

# [TOP 8] Nächste Vorstandssitzungen

- Plan: Jeweils am Dienstag nach den Community-Treffen
- Nächster Termin: 14.8.

# [TOP 9] Politische Arbeit

- Im Rahmen politischer Arbeit ist ein Amt im F3N nicht zu Werbezwecke zu nutzen
- Eine Nennung aus Transparenzgründen ist in Ordnung

