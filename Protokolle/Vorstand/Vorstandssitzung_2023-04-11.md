Zeit: 11.04.2023, Start: 19:30 Uhr, Ende: 19:35 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Dennis
- Matthias

__Abwesend__
- Jan
- Arne
- Sarah
- Malte

---

# [TOP 0] Formalia
- Vorstand unvollständig anwesend
- Vorstand nicht beschlussfähig
  - Vorstandssitzung nicht durchgeführt
- Einladung erfolgte ordnungsgemäß am 04.04.2023 per E-Mail

# [TOP 1] Annahme des Protokolls vom 14.03.2023
Vorstandssitzung ausgefallen, kein Ergebnis

# [TOP 2] Mitgliedantrag
Vorstandssitzung ausgefallen, kein Ergebnis

# [TOP 3] MV2023 Vorbereitung
1. Präsentation
2. Kassenprüfung/Kassenbericht
3. GitLab Seite für Informationen

Vorstandssitzung ausgefallen, kein Ergebnis

# [TOP 4] Verschiedenes & Termine
Vorstandssitzung ausgefallen, kein Ergebnis
