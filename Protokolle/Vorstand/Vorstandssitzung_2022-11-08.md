Zeit: 08.11.2022, Start: 19:30 Uhr, Ende: 19:40 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis

__Abwesend__

---

# [TOP 0] Formalia
- Vorstand vollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 01.11.2022 per E-Mail

# [TOP 1] Annahme des Protokolls vom 11.10.2022
- Korrigiert
- einstimmig angenommen

# [TOP 2] Steuererklärung
- Jan hat die Steuererklärung nach Vorbild der vorherigen Steuererklärung
  erstellt
- Jan verschickt die Steuererklärung in Kürze

# [TOP 3]  Verschiedenes und Termine
- nächste Vorstandssitzung 13.12.2022
