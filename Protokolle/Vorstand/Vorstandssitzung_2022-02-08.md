Zeit: 08.02.2022, Start: 19:35 Uhr, Ende: 20:15 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---


# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 01.02.2022 per E-Mail


# [TOP 2] Annahme Protokoll vom 11.01.2022

- TOP1 korrigiert
- TOP4 korrigiert
- mit einer Enthaltung angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen 02/2021

- ist vorbereitet, kann Korrektur gelesen werden
- der Bericht soll am 22.02.2022 rausgeschickt werden


# [TOP 4] Mitgliedervollversammlung 2022

- wie ist die Regelung bzgl. Online-Versammlung?
- Online-Versammlung wird bevorzugt, weil die Corona-Situation keine genaue
  Planung zulässt
- ein Vorschlag war den Termin nach Ostern zu wählen
- Malte erstellt eine Umfrage zur Terminfindung
  - diese muss bis Ende dieser Woche bereits beendet sein
- Info: Kassenbericht muss 2 Wochen vor der Versammlung fertig sein


# [TOP 5] Sponsoren und weitere Fördermitglieder finden

- falls der Weihnachtsmarkt dieses Jahr stattfinden sollte, hat Arne eine
  Möglichkeit für Uplink organisiert


# [TOP 6] Verschiedenes und Termine

- es gab eine Anfrage, ob wir auf unserer Infrastruktur eine Seite hosten
  können. Die Anfrage war zu ungenau um das bewerten zu können. Daher
  fragen wir nochmal nach.
