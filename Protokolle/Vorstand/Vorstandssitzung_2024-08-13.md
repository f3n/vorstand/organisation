Zeit: 13.08.2024, Start: 19:10 Uhr, Ende: 19:40 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Arne

---

__Anwesend__
- Arne
- Josha
- Stefan
- Jan
- Florian
- Felix

__Abwesend__
- Malte
- Dennis

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 06.08.2024 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 09.07.2024
  - einstimmig angenommen

# [TOP 2] Nachbereitung MV

- Aachener Bank
  - vertröstet auf nach den Sommerferien

# [TOP 3] Rechenschaftsbericht H1/2024

- sollte im August rausgehen an die Stadt
- es soll deutlich gesagt werden, dass wir weiterhin auf das Geld der Stadt angewiesen sind und um Auschüttung gebeten wird

# [TOP 4] Verschiedenes und Termine

- ausstehende Mitgliedsbeiträge wurden eingetrieben

- Artikel für den Freifunk Blog über was Freifunk kostet
