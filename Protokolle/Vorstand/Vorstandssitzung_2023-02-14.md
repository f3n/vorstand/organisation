Zeit: 14.02.2023, Start: 19:30 Uhr, Ende: 19:55 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Dennis
- Sarah
- Malte

__Abwesend__

---

# [TOP 0] Formalia
- Vorstand vollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 07.02.2023 per E-Mail

# [TOP 1] Annahme des Protokolls vom 10.01.2022
- zwei Enthaltungen, drei dafür
  - Protokoll angenommen

# [TOP 2] Rechenschaftsbericht 02/2022
- Sarah möchte das bis Ende März abschicken
- Jan muss noch Zahlen liefern
- Arne hat die Grafik schon zur Verfügung gestellt

# [TOP 3]  Wiederaufnnahme Matthias in den Vorstand
- einstimmig angenommen

# [TOP 4]  MV2023
- Wer steht wieder zur Verfügung?
  - Jan würde wie bereits früher angekündigt den Kassenwart abgeben
- geplanter Termin: 20.04.2023 19:00Uhr
- Versammlung muss dieses Jahr wieder offline durchgeführt werden
  - Location muss noch organisiert werden
- Sarah bereitet Einladung vor
  - muss 4 Wochen vorher verschickt werden (16.03.2023)
- Jan braucht ggfls. Unterstützung bei Kassenprüfung
  - Bericht muss 2 Wochen vor MV verschickt werden (06.04.2023)

# [TOP 5]  Verschiedenes und Termine
- Geld für neue Freifunk-Sticker
  - Sticker sollen ohne Community-Bezeichnung bestellt werden
- Ehrenwert
  - Sarah hat online keine Informationen finden können
  - Sarah vermutet, dass die Veranstaltung nicht mehr stattfinden wird
  - Sarah möchte evtl. die Stadt anschreiben und nachfragen
