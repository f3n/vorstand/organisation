Zeit: 14.03.2023, Start: 19:40 Uhr, Ende: 20:25 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Dennis
- Sarah
- Malte
- Matthias

__Abwesend__

---

# [TOP 0] Formalia
- Vorstand vollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 07.03.2023 per E-Mail

# [TOP 1] Annahme des Protokolls vom 14.02.2023
- einstimmig

# [TOP 2] Rechenschaftsbericht 02/2022
- Rückmeldung der Stadt Aachen erhalten
  - es wurde der noch offene Restbetrag der Stadt Aachen genannt

# [TOP 3] MV Vorbereitungen
## 3a Raum
- Relaix
  - ist aktuell die beste Option
  - man kann einen Fahrservice zw. Bahnhof<>Relaix anbieten

## 3b Einladung
- muss spätestens Sonntag 19.03.2023 verschickt werden
- spätestens bis Samstag Abend Korrekturlesen
- Sarah macht den Entwurf und verschickt die Einladung
- MV ist geplant für: 20.04.2023 19:00Uhr

## 3c Vorstand-Zusammensetzung
- Vorsitz 1: Arne
- Vorsitz 2: Dennis
- Kasse: Matthias
- Beisitzer: Stefan
- Beisitzer: Florian
- Beisitzer: Malte
- Beisitzer: Jan

## 3d Präsentation + Jahresbericht
- Dennis erstellt Entwurf
- Entwurf bis 05.04.2023 wünschenswert

## 3e Kassenprüfung / Kassenbericht
- Kassenprüfer: Daniel + Matthias

## 3f GitLab Seite für Informationen 
- Sarah wird diese wieder erstellen

## 3g Team-Berichte
- Team-Berichte
  - Team Server/Backbone: Florian?
  - Team Düren: Matthias

# [TOP 4] Verschiedenes und Termine
- Eherenwert
  - weiterhin nichts neues
