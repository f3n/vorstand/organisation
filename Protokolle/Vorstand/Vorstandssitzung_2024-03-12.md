Zeit: 12.03.2024, Start: 19:40 Uhr, Ende: 20:48 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Malte
- Dennis
- Stefan
- Matthias

__Abwesend__
- Arne
- Florian

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 05.03.2024 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 13.02.2024
  - mit einer Enthaltung angenommen

# [TOP 2] Vorbereitung MV 2024

- RelAix Besprechungsraum ist reserviert

- Einladung sollte am besten heute Abend verschickt werden, wenn wir die MV am
  10.04.2024 halten wollen

- Kassenprüfung muss bis 2 Wochen vor der MV stattgefunden und per Rundmail
  verschickt wurden sein

- steht der Vorstand vollständig zur Wiederwahl?

- Alternativangebote Server-Housing einholen
  - bisher noch keine Angebote eingeholt
  - sollte vor der MV eingeholt werden
  - Malte kümmert sich drum

- Präsentation vorbereiten
  - Dennis kümmert sich drum

- Jahresbericht vorbereiten
  - Dennis kümmert sich drum

# [TOP 3] Supernodes Hardware

- SSDs wurden bestellt und sind eingetroffen
  - wie sieht es mit der Bezahlung aus?
  - wie ist der Stand?

# [TOP 4] Rechenschaftsbericht

- Rechenschaftsbericht H2/2023
  - hat Arne vorbereitet, Kritik erwünscht

# [TOP 5] Verschiedenes und Termine

- HW-Spenden
  - Einzelfallbetrachtung
  - Rechnung muss vorhanden sein
  - der Aufwand lohnt sich nur bei größeren Installationen/Investitionssummen

- FF-Solingen
  - hat sich aufgelöst
  - wir haben die Geräte von FF Solingen in unsere Infrastruktur migriert
  - aktuell werden die Geräte (~23Stück) nur mit der MAC-Adresse auf der Karte
    angezeigt
  - falls die Metadaten von FF Solingen noch nachgeliefert werden, werden
    die Geräte damit wieder befüllt
  - die Erfahrung, welche hier gesammelt wurde, wird nun bei FFMUC eingesetzt

- UK Kalverbenden
  - Gespräche mit der Stadt gehalten
  - Infrastruktur in der UK war offline und es wurde von den Bewohnern viel an
    der Verkabelung rumgespielt
  - Verkabelung wurde erneuert und Infrastruktur ist jetzt wieder online
  - Geräte haben jetzt Sticker mit Kontakt-E-Mail-Adresse
  - es wurde eine kleine Doku auf Papier erstellt und vor Ort übergeben
  - 16MBit Leitung für das gesamte UK, möglich wären 50MBit+ laut
    Internetvergleich

- Nachfolgeevent Ehrenwert "Aachen zeigt Engagement"
  - Standort: Kurpark
  - Datum: 23.06.2024
  - Link: https://www.aachen.de/DE/stadt_buerger/gesellschaft_soziales/ehrenamt/02_Aachen-zeigt-Engagement/index.html
  - Anmeldefrist bis 10.04.2024
  - wir wurden von der Stadt eingeladen
  - die Stadt würde sich darüber freuen, wenn wir während des Events Freifunk
    auf dem Kurpark anbieten können
  - Unklar woher wir Strom bekommen
  - wir benötigen mehrere Geräte zum Ausleuchten, woher?
  - als Uplink-Möglichkeiten gibt es den Congress und auch die Feuerwehr
    (Starlink). Alternativ ein LTE-Router
