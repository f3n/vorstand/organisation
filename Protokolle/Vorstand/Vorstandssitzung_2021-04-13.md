Zeit: 13.04.2021, Start: 19:40 Uhr, Ende: 20:10 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 06.04.2021 per E-Mail.


# [TOP 2] Annahme des Protokolls vom 09.03.2021

- einstimmig angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen

- Alle sollen nochmal drüber schauen und bei Bedarf bis Freitag Rückmeldung
  an Sarah geben.
- Sarah verschickt den Bericht ohne weitere Rückfrage am kommen Freitag oder
  Montag.


# [TOP 4] Mitgliedervollversammlung 2021

- Vorbereitung + Aufgabenverteilung
  - Präsentation (Dennis)
    - Dennis erstellt diese
  - Jahresbericht
    - Dennis hat diesen vorbereitet
    - die übrigen Mitglieder prüfen diesen noch einmal und geben ggfs.
      Rückmeldung
  - Kassenbericht geprüft?
    - Liegt bei den Prüfern. Erste Rückmeldungen sind positiv.
    - Bisher keine Rückmeldung in Form einer verbindlichen Prüfung.
    - Die Spende an FFRL wird jetzt als separater Posten dargestellt.
    - Jan kümmert sich um die Unterschrift und verschickt diese anschl.
      fristgerecht an die Mitglieder


# [TOP 5] Verschiedenes und Termine 

## 5.1 Generalprobe 27.04.2021 19.30Uhr

## 5.2 Mitgliedervollversammlung 29.04.2021 20.00Uhr

- Es muss ein Raum im voraus erstellt werden, damit man Moderator ist
  und so Probleme vermeiden kann.



