Zeit: 09.07.2024, Start: 19:10 Uhr, Ende: 20:03 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Josha
- Felix
- Dennis
- Stefan
- Florian

__Abwesend__
- Jan
- Malte

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 28.06.2024 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 14.05.2024
  - einstimmig angenommen
- Vorstandssitzung 11.06.2024
  - einstimmig angenommen

# [TOP 2] Nachbereitung MV

- Rechnung vom Notar und Amtsgericht bei Arne eingetrudelt
- Vereinsregister
  - fertig
- Aachener Bank
  - bisher keine Rückmeldung bzgl. Termin
  - Zugang steht noch immer aus

# [TOP 3] Rechenschaftsbericht H1/2024

- aktuelles Geld der Stadt Aachen reicht noch bis Jahreende

# [TOP 4] Aachen zeigt Engagement

- war unterm Strich sehr erfolgreich
  - Gäste haben Interesse an Freifunk gezeigt
- die Besetzung war Minimalbesetzung und sollte beim nächsten mal größer
  ausfallen

# [TOP 5] Verschiedenes und Termine

- Steuerberater und SSDs wurden inzwischen per Überweisungsträger bezahlt

- Weltfest
  - Stand wie bei "Aachen zeigt Engagement" aufgebaut
  - mehrere Freifunk-Router zum Ausleuchten des Festes aufgebaut
  - es war gut, dass wir einen eigenen Stand hatten
  - Stefan muss noch Hardware abholen
