Zeit: 13.07.2021, Start: 19:30 Uhr, Ende: 20:00 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)


Protokoll: Dennis

---

__Anwesend__
- Arne
- Malte
- Sarah
- Dennis
- Matthias
- Jan

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 06.07.2021 bei der Vorstandssitzung.


# [TOP 2] Annahme des Protokolls vom 08.06.2021

- mit einer Enthaltung angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen

- bisher keine Neuigkeiten


# [TOP 4] Mitgliedervollversammlung Notar

- Jan macht eine Konsolidierte Fassung der Satzung
- Jan muss noch unterschreiben
- Jan nennt Sarah einen Termin
- Jan leistet seine Unterschrift beim Notar vor Ort


# [TOP 5] Antrag Welthaus

- die aktuellste Fassung muss nochmal durchgelesen und bewertet werden


# [TOP 6] Verschiedenes und Termine

- Klimacamp-Anfrage im Slack wurde noch nicht beantwortet
  - Malte moechte eine Antwort senden
- Die naechste Vorstandssitzung findet erst am 08.09.2021 statt
