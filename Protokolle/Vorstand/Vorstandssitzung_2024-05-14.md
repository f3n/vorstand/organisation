Zeit: 14.05.2024, Start: 19:40 Uhr, Ende: 21:20 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Felix
- Dennis
- Stefan
- Florian

__Abwesend__
- Josha
- Malte

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 07.05.2024 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 12.03.2024
  - mit einer Enthaltung angenommen

# [TOP 2] Nachbereitung MV

- Notar
  - Arne hat mit dem Notar gesprochen
  - MV-Protokoll schon beim Notar
  - unterschriebenes MV-Pro
- Aachener Bank
  - der aktuelle Login wurde als 4-Augen-Prinzip eingerichtet und ist somit
    relativ nutzlos für uns
  - der Eintragungsfehler lässt sich aktuell nicht Rückgängig machen
    - vor Ort-Termin notwendig
  - offene Rechnungen können dadurch aktuell nur über Umwege beglichen werden
  - Arne ruft morgen die Steuerberaterin an und schildert unsere Situation

# [TOP 3] Rechenschaftsbericht

- Jan muss noch Zahlen nachtragen, weil er als einziger (lesend) Bankzugang hat
- Zahlen sollten im Kassenbericht der MV enthalten sein

# [TOP 4] Aachen zeigt Engagement

- folgende Materialien wurden im Keller von Stefan gesichtet:
  - Beachflag
  - weißer Stehtisch
  - die großen Freifunk-Banner
- folgendes wird noch benötigt
  - Pavillon
  - weiterer Stehtisch
  - Kasten Wasser
- Budget-Festlegung für das Event: 300€
  - dieses Budget steht insgesamt allen Vorstandmitgliedern zur Verfügung
  - einstimmig angenommen

# [TOP 5] Verschiedenes und Termine

- Austritt eines Mitglieds
  - Kündigung zum Jahresende vom Vorstand bestätigt

- nächster Stammtisch 28.05.2024 19:30Uhr findet im Hotel zur Brücke in
  Herzogenrath statt

- Spendenquittung (Bestätigung einer Sachzuwendung) Paul für Sticker
  - Aufgabe für den Kassenwart
  - Formular 015 kann hier gefunden werden: https://www.formulare-bfinv.de
    - es muss geprüft werden, ob das das richtige Formular ist

- Treffen bei der UK Kalverbenden hat stattgefunden
  - weiteres Treffen geplant

- gitlab-Zugang für Josha
  - Jan kümmert sich drum

- Unterstützung durch die Stadt Aachen
  - Arne fragt mit dem Senden des nächsten Rechenschaftsberichts nach, wie der
    Status der finanziellen Unterstützung aussieht

- Freifunk Festival 2024 in Hassfurt 13.-15.09.2024
