Zeit: 08.10.2024, Start: 19:08 Uhr, Ende: 19:48 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Jan     (ab 19:30Uhr)
- Josha   (ab 19:30Uhr)
- Florian (ab 19:30Uhr)
- Arne
- Malte
- Felix
- Dennis
- Stefan

__Abwesend__

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 01.10.2024 per Mail


# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 10.09.2024
  - einstimmig angenommen


# [TOP 2] Nachbereitung MV

- Aachener Bank
  - Termin Corona-bedingt ausgefallen
  - man kann über einen Filialwechsel nachdenken


# [TOP 3] Rechenschaftsbericht H1/2024

- Entwurf liegt in Git
  - Jan muss noch Zahlen (Ausgaben) nachtragen bzw. mitteilen
  - Josha schaut, ob er diese Daten selber nachschauen kann
- der Rechenschaftsbericht enthält eine Info, dass das Stadt-Geld zum Jahresende
  ausläuft
- Nachtrag der bisher durchlaufenen, gestarteten oder -planten Projekte mit der
  Stadt sinnvoll


# [TOP 4] Verschiedenes und Termine

- Ehrenamtstag Jülich
  - findet am 26.10.2024 von 11:00 bis 15:00 Uhr im Pädagogischen Zentrum (PZ)
    des Gymnasium Zitadelle in Jülich statt
  - wenig Strom pro Stand
  - keine WLAN-Versorgung vorhanden
  - LTE-Empfang in Halle vorhanden
  - vorab findet ein jistsi-Termin mit FF-Jülich statt

- Couvenhalle
  - soll ein öffnetliches WLAN bekommen
    - Stadt-IT hat uns angefragt, weil diese selber nicht genug Ressourcen
      dafür hat
  - Denkmalgeschützter Bau
  - es muss noch ein Termin zur Begehung ausgemacht werden

- Nachweis Gemeinnützigkeit
  - vorhandener Nachweis ist noch gültig
  - Felix lädt das im Portal hoch

- Ferienhaus "Zum Sonnenschein" mit mehreren Ferienwohnungen wurde mit Freifunk
  ausgestattet

- Problem mit einem Netzwerkport auf dem Blech
  - bisher keine neue Erkenntnis

- Amazon Alexa funktioniert nicht mehr im Freifunk-Netz
  - bisher keine Verbesserung

- es gab eine Rückmeldung, dass ein Tesla aus dem Freifunk-Netz nicht
  Fernbedient werden kann
  - unklar, wie lange dieses Problem besteht
