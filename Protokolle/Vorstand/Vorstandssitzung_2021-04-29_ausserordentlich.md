Zeit: 29.04.2021, Start: 20:03 Uhr, Ende: 20:05 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.f3n-ac.de/F3N-MV-2021)

Protokoll: Sarah

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Sitzung fand statt unter Verzicht auf die fristgerechte Einladung.

# [TOP 2] Bestätigung der außerordentlichen Sitzung
- Vorstand stimmt über die Möglichkeit der außerordentlichen und kurzfristigen
  Sitzung ab
- Der Vorstand ist mit der Durchführung einer kurzfristigen und
  außerordentlichen Sitzung einverstanden 
- einstimmig angenommen


# [TOP 3] Annahme des Mitgliedsantrags

- Es wird über einen Mitgliedsantrag zur Beitreteung des Vereins abgestimmt.
- Das Mitglied wurde einstimmig bestätigt.
