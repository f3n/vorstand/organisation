Zeit: 12.04.2022, Start: 19:40 Uhr, Ende: 20:05 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---

__Anwesend__
- Jan
- Malte
- Sarah
- Dennis
- Matthias

__Abwesend__
- Arne

---


# [TOP 1] Formalia

- Vorstand unvollständig anwesend
  - Vorstand ist Beschlussfähig
- Einladung erfolgte ordnungsgemäß am 05.04.2022 per E-Mail


# [TOP 2] Annahme Protokoll vom 08.03.2022

- einstimmig angenommen


# [TOP 3] Mitgliedervollversammlung 2022

- Malte erstellt eine neue Umfrage zur Terminfindung
  - bei den ersten Terminen konnten wir den Vorstand nicht zusammenbringen 


# [TOP 4] Verschiedenes und Termine

- Jan hat Erinnerungsmails für fällige Mitgliedsbeiträge verschickt
  - inzwischen wurden sehr viele fällige Beiträge überwiesen
- es wurden diverse Flüchtlingsunterkünfte mit Freifunk versorgt
- Gespräch mit der Steuerberaterin steht noch aus
