Zeit: 10.12.2024, Start: 19:26 Uhr, Ende: 20:06 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Jan Niehusmann

---

__Anwesend__
- Arne (ab 19:46 / TOP 4)
- Felix
- Jan
- Josha
- Malte
- Stefan

__Abwesend__
- Dennis
- Florian

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 1.12.2024 per Mail.
- Die Sitzung ist beschlussfähig

# [TOP 1] Annahme von Protokollen

Das Protokoll der Vorstandssitzung vom 12.11.2024 wird einstimmig bei
einer Enthaltung angenommen.

# [TOP 2] Stand städtischer Förderung

Die bisherige Förderung wurde nicht automatisch verlängert. Der Haushalt
für 2025 ist noch nicht beschlossen, es bestehen also noch Chancen auf
eine weitere Förderung. Hierzu könnten Verwaltung sowie der Ausschuss
für Wissenschaft und Digitalisierung kontaktiert werden. Malte kümmert
sich darum bzw. organisiert, dass sich jemand darum kümmert.

# [TOP 3] Bericht Freifunktag & FF Rheinland MV

Stefan und Felix berichten vom Freifunktag und der FF Rheinland
Mitgliederversammlung.

# [TOP 4] Verschiedenes und Termine

Am 17.12. um 18:30 treffen wir uns am Öcher Glühweintreff zur
Weihnachtsfeier des F3N und Freifunk Aachen. Es wird abgestimmt, für
die Weihnachtsfeier bis zu 200€ aus Vereinsmitteln bereitzustellen. Mit
einer Enthaltung angenommen.
