Zeit: 09.02.2021, Start: 19:45 Uhr, Ende: 20:30 Uhr

Ort: Online-Meeting

Protokoll: Dennis

---
__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis

__Abwesend__
- Matthias

---

# [TOP 1] Formalia
- Einladung erfolgte ordnungsgemäß am 02.02.2021 per E-Mail


# [TOP 2] Annahme des Protokolls vom 12.01.2021
- Einstimmig angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen
- Sarah schreibt einen Entwurf
- Jan sucht Zahlen raus


# [TOP 4] Förderung NRW für Server - Abrechnung
- Das Land meldet sich zeitnah zurueck, ob die Abrechnung
  angekommen ist.
- Den aktuell bekannten Beitrag zur Rueckzahlung kann bereits zuruck gezahlt
  werden. Dennis gibt dazu die notwendigen Info's an Jan.


# [TOP 5] Gemeinnützigkeit für Freifunk
- Unsere Satzung muss einer Mustersatzung entsprechen. Auf den ersten Blick
  scheint dies der Fall zu sein.
- Sarah fragt bei der Stadt Aachen nach, ob sich dadurch fuer uns Aenderungen
  ergeben
- Erwaehnung bei der Einladung zur Mitgliederversammlung


# [TOP 6] Mitgliedervollversammlung 2021 im Q1

## [TOP 6.1] Termin
- Die Mitgliederversammlung soll am 25.03.2021 20Uhr stattfinden.
- Jan laedt ein.

## [TOP 6.2] Vorbereitung
- Einladung muss spaetestens am 04.03.2021 verschickt werden.
- Die Versammlungsthemen muessen vor der naechsten Vorstandsversammlung
  versendet werden.
- Fuer die Vorbereitung der Einladung wird keine separate Vorstandsversammlung
  abgehalten.
- Die Erstellung der Praesentation wird bei der naechsten Vorstandsversammlung
  festgelegt.


# [TOP 7] Verschiedenes und Termine
- Es hat sich eine Gruppe von Leuten ueber mehrere Freifunkvereine gebildet,
  welche Moeglichkeiten zur Mitglieder-Anwerbung bespricht.
- Der Foerderantrag fuer das Projekt Welthaus muss vom F3N beim Land NRW
  gestellt werden.
  - Vorstellung bei Mitgliederversammlung
