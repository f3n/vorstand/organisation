Zeit: 14.09.2021, Start: 19:55 Uhr, Ende: 20:02 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)


Protokoll: Jan

---

__Anwesend__
- Arne
- Malte
- Dennis
- Jan

__Entschuldigt__
- Sarah

__Fehlt__
- Matthias

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 07.09.2021 bei der Vorstandssitzung.

# [TOP 2] Annahme des Protokolls vom 13.07.2021

- mit einer Enthaltung angenommen

# [TOP 3] Rechenschaftsbericht Stadt Aachen

- bisher keine Neuigkeiten bezüglich des Rechenschaftsberichts
- 6000€ sind von der Stadt heute überwiesen worden, wie abgesprochen

# [TOP 4] Mitgliedervollversammlung Notar

- Antrag auf Gemeinnützigkeit ist an das Finanzamt gemailt
- Notar hat Anmeldung vorbereitet, Sarah und Jan müssen noch unterschreiben

# [TOP 5] Antrag Welthaus

- die aktuellste Fassung muss nochmal durchgelesen und bewertet werden

# [TOP 6] Verschiedenes und Termine

- keine Themen
- nächste Sitzung voraussichtlich 12.10.2021

